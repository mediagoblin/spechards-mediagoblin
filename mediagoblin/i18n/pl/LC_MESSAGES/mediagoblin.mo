��    �     $              ,  =   -  ;   k  8   �  z   �  ?   [  �   �  o      A  �   L   �$    %  �  +'  {   )  �  �)  �   {+  H   m,  M   �,     -     -     %-     7-     I-     Z-     n-     �-     �-     �-     �-  0   �-  ?   '.     g.     ~.     �.     �.     �.     �.     �.     �.  >   �.  2   6/  5   i/  /   �/  Z   �/     *0     E0  �  \0  9   �1     2     52     B2     W2     v2     �2     �2     �2     �2     �2     �2     �2     �2  	   3     3     3     )3     =3  )   L3     v3     |3  '   �3     �3     �3  6   �3  +   4  H   =4  J   �4  5   �4     5  .   5  	   G5     Q5     ]5     k5     �5  	   �5      �5     �5     �5     �5     �5     �5     �5     6  �   6     �6  "   �6     �6     �6     7  (   $7     M7      k7     �7     �7     �7  
   �7     �7     �7     �7  %   �7  !   8  4   ;8     p8  z   �8  )   
9     49     ;9     I9     \9     r9     �9     �9     �9     �9     �9     �9     �9     :  &   :     ::     L:     _:     r:     y:     ~:     �:     �:     �:     �:     �:     ;  )   ,;     V;     _;     n;  	   s;     };     �;     �;     �;     �;  '   �;     <     <  (   (<     Q<  )   k<  *   �<  6   �<  2   �<  Z   *=     �=  S   �=  ?   �=  <   !>  4   ^>     �>     �>     �>     �>     �>  .   �>  �   ?     �?     �?     @     @  G   @  ,   a@  }   �@  b   A  +   oA  �   �A     �B  O   �B     �B  "   C  4   7C     lC  8   �C     �C  
   �C  ~   �C  y   RD     �D     LE     fE     �E     �E  #   �E  "   �E  .   �E     F     8F     @F     SF     \F     cF     �F     �F     �F     �F     �F     �F     �F     
G     G     $G     -G     KG     ]G     zG  *   G     �G     �G     �G     �G  "   �G  '   �G  "   $H     GH     `H     sH     �H     �H     �H     �H     �H     I     I     *I  #   3I     WI  	   dI     nI     tI     �I      �I     �I     �I     �I     �I     J      J     9J     SJ     \J     jJ     sJ  5   |J  	   �J     �J  (   �J     �J  �   K     �K  	   �K     �K     �K     �K     �K  $   �K  B   L  8   HL     �L     �L     �L     �L  �   �L     KM  0   RM     �M     �M  	   �M     �M     �M     �M     �M     �M     �M     N     #N     4N     KN     fN     �N     �N     �N     �N     �N     �N     �N     �N  	   O  1   O     AO     SO     kO     �O     �O     �O     �O     �O     �O  _   �O  W   OP  �   �P  (   MQ  5   vQ  ,   �Q  7   �Q  >   R  H   PR  3   �R     �R     �R     S  .   #S  1   RS  .   �S  +   �S      �S  X    T  Z   YT  k   �T  c    U  ?   �U  *   �U     �U     �U     V  �   #V     �V     �V     �V     �V     �V  .   �V  =   W  3   UW  4   �W  #   �W  K   �W  E   .X  F   tX     �X  n   �X  A   GY     �Y     �Y  S   �Y  N   Z     ]Z  ,   wZ  -   �Z  �   �Z  .   t[      �[     �[  )   �[  )   \  %   5\  T   [\  �   �\  /   1]  `   a]  2   �]     �]  ^   �]     Z^     ^^  
   c^  	   n^     x^     �^  
   �^     �^     �^     �^     �^     �^     _     
_     !_     ?_     R_     k_      p_     �_  /   �_     �_     �_     �_     `     `  0   #`  7   T`  #   �`     �`     �`     �`     �`  $   �`     !a     0a     4a     Ra  *   oa     �a  U   �a  B    b  H   Cb  C   �b  7   �b  @   c  ;   Ic     �c  ]   �c  {   �c  |   td  �   �d  �   e  +   f  #   1f  G   Uf  �   �f  �   4g  �   �g  u   jh  D   �h  P   %i  /   vi     �i     �i  )   �i     j     j  &   2j  :   Yj     �j  1   �j  6   �j  .  k  +   El     ql  '   �l  2   �l     �l  %   �l  ^   #m     �m  &   �m     �m     �m     �m     �m  	   n     n     $n     1n     9n     @n     Fn     Ln  &   Rn     yn     �n     �n  F   �n  	   �n  8   �n  =   "o  �  `o  =   Wq  ;   �q  8   �q  z   
r  ?   �r  �   �r  o   Gs  A  �s  L   �w    Fx  �  Uz  {   A|  �  �|  �   �~  H   �  M   �     .�     ?�     O�     a�     s�     ��     ��     ��      Ҁ     �     �  9   #�  I   ]�     ��     ��  "   ف     ��      �     =�     E�     G�  >   U�  .   ��  =   Â  :   �  e   <�     ��     ��  �  ԃ  9   o�     ��     Å     Ѕ     �     �     �     �  =   %�     c�     s�     ��     ��     ��     ��     ʆ     ֆ     �     ��  '   
�     2�     9�  '   U�     }�     ��  2   ��  +   Շ  3   �  J   5�  8   ��     ��  6   ˈ     �     �     �     ,�     J�     h�      v�     ��     ��     ��     ŉ     ى     �     ��  �   ��     ��  5   Ê     ��     �     #�  #   9�  5   ]�  )   ��     ��     ΋     ݋     ��      �  	   �     )�  &   =�  3   d�  G   ��  (   ��  �   	�  9   ��     ֍     ލ     �     ��     �     %�     8�     Q�  	   j�     t�     z�  =   ��     Ď  &   ׎     ��     �     �     2�     9�     A�     F�     \�     n�     }�     ��     ��  )   Ϗ     ��     �     �  	   �      �     .�     <�     X�     s�  (   ��     ��     Ԑ  9   �  #   �  *   ?�  *   j�  @   ��  J   ֑  Z   !�     |�  S   ��  ?   ْ  <   �  4   V�     ��     ��     ��     ��     ��  .   ˓  �   ��     �  	   ��     �     �  @   �  -   [�  �   ��  f   �  &   ��  �   ��     ��  O   ��     
�  "    �  ?   C�     ��  .   ��     ͘  
   И  �   ۘ  �   ��     �     ��     ��     Ԛ     �  J   ��  ,   D�  .   q�     ��     ��     ƛ     ؛     �  "   �     �     !�  !   ?�     a�     w�     ��  "   ��     ɜ     ڜ     �     �     
�     �     5�  *   ;�     f�      v�     ��     ��  $   ��  '   ˝  %   �     �     2�  )   R�     |�  )   ��  !   ��     ߞ     �     �     $�     7�  -   @�     n�     {�     ��  +   ��     ��  +   ��     �     �      �     )�     G�     b�  !   ��  	   ��     ��     ��     Ơ  5   ͠  	   �     �  )   �  $   C�  �   h�     �  	   !�     +�     C�     K�     T�  $   q�  e   ��  9   ��     6�     =�     R�     b�  �   u�     �  0   �     K�     R�  	   l�     v�     ��     ��     ��     ��     Ť     ֤     �     ��     �  %   .�  &   T�     {�     ��     ��     ��     ��     Υ     ץ  	   �  +   �     �     .�  !   F�     h�     u�  	   ��     ��  "   ��     ئ  _   ݦ  N   =�  �   ��  0   6�  8   g�  3   ��  B   Ԩ  >   �  H   V�  :   ��     ک     ��     �  .   1�  5   `�  .   ��  2   Ū      ��  X   �  �   r�  k   ��  n   d�  ?   Ӭ  *   �     >�     E�  !   Z�  �   |�  
   �  	   �     �     +�     <�  8   E�  N   ~�  8   ͮ  4   �  "   ;�  Q   ^�  P   ��  O   �     Q�  o   e�  O   հ     %�     ?�  F   [�  U   ��  0   ��  @   )�  D   j�  �   ��  %   D�  (   j�     ��  D   ��  /   �  ;   "�  G   ^�  �   ��  ?   (�  f   h�  ;   ϵ     �  w   �     ��     ��  
   ��  	   ��     ��     ��  
   ϶     ڶ     ��     �  +   �     @�     ]�     c�  '   {�     ��  "   ·     �  0   �     �  1   2�     d�     q�     w�     ��     ��  0   ��  7   �  #   #�     G�     S�     a�     ��  $   ��     ��     ˹  *   Ϲ     ��     �     1�  S   A�  B   ��  J   غ  F   #�  =   j�  @   ��  <   �     &�  ]   >�  {   ��  �   �  �   ��  �   8�  7   �  1   �  E   N�  �   ��  �   +�  �   ��  �   [�  D   ��  �   *�  /   ��     ��     ��  %   �     >�     X�  #   m�  C   ��     ��  6   ��  +    �  .  L�  .   {�     ��  .   ��  T   ��  %   A�  7   g�  }   ��  #   �     A�     a�     t�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��  /    �     0�     E�     N�  F   R�  
   ��  8   ��  N   ��   
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            ❖ Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   to access your account?  "%s" added to collection "%s" "%s" already in collection "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (%(username)s's collection) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s ago %(username)s's Privileges %(username)s's collections %(username)s's media %(username)s's profile (remove) + -- Select -- -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a>'s account <a href="%(user_url)s">%(username)s</a>'s collections <a href="%(user_url)s">%(username)s</a>'s media <a href="%(user_url)s">%(username)s</a>'s media with tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). A collection with that slug already exists for this user. Account settings saved Action Taken Active Reports Filed Active Reports on %(username)s Active Users Add Add Blog Post Add a Persona email address Add a collection Add a comment Add a new collection Add an OpenID Add attachment Add media Add new Row Add this comment Add to a collection Add your media Add “%(media_title)s” to a collection Added All reports on %(username)s All reports that %(username)s has filed All rights reserved Allow Almost done! Your account still needs to be activated. An acceptable processing file was not found An email has been sent with instructions on how to change your password. An email should arrive in a few moments with instructions on how to do so. An entry with that slug already exists for this user. An error occured Applications with access to your account can:  Atom feed Attachments Authorization Authorization Complete Authorization Finished Authorize BANNED until %(expiration_date)s Bad Request Ban User Ban the user Banned Indefinitely Bio Browse collections CAUTION: CSRF cookie not present. This is most likely the result of a cookie blocker or somesuch.<br/>Make sure to permit the settings of cookies for this domain. Cancel Cannot link theme... no theme set
 Change account settings Change your information Change your password. Changing %(username)s's account settings Changing %(username)s's email Changing %(username)s's password Clear empty Rows Closed Reports Collected in Collection Collection "%s" added! Comment Comment Preview Copy and paste this into your client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Could not read the image file. Could not send password recovery email as your username is inactive or your account's email address has not been verified. Couldn't find someone with that username. Create Create a Blog Create an account! Create new collection Create one here! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Created Delete Delete Blog Delete a Persona email address Delete an OpenID Delete collection %(collection_title)s Delete my account Delete permanently Delete the content Demote Deny Description Description of Report Description of this collection Description of this work Do you want to authorize  Don't have an account yet? Don't have one yet? It's easy! Don't process eagerly, pass off to celery Download Download model Edit Edit Blog Edit Metadata Edit profile Editing %(collection_title)s Editing %(media_title)s Editing %(username)s's profile Editing attachments for %(media_title)s Email Email address Email me when others comment on my media Email verification needed Enable insite notifications about events. Enter the URL for the media to be featured Enter your old password to prove you own this account. Enter your password to prove you own this account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Explore FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File File Format File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Forgot your password? Front Go to page: Granted Here you can track the state of media being processed on this instance. Here's a spot to tell others about yourself. Hi %(username)s,

to activate your GNU MediaGoblin account, open the following URL in
your web browser:

%(verification_url)s Hi %(username)s,
%(comment_author)s commented on your post (%(comment_url)s) at %(instance_name)s
 Hi there, welcome to this MediaGoblin site! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 I am sure I want to delete this I am sure I want to remove this item from the collection ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. If you are that person but you've lost your verification email, you can <a href="%(login_url)s">log in</a> and resend it. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out In case it doesn't: Include a note Invalid User name or email address. Invalid file given for media type. Is there another way to manage featured media? Last 10 successful uploads License License preference Location Log in Log in to create an account! Log out Logging in failed! Mark all read Max file size: {0} mb Media in-processing Media processing panel Media tagged with: %(tag_name)s MediaGoblin logo MetaData Metadata Metadata for "%(media_name)s" Most recent media Must provide an oauth_token. Name Name of user these media entries belong to New comments New email address New password No No OpenID service was found for %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. No failed entries! No media in-processing No open reports found. No processed entries, yet! No request token found. No users found. Nothing is currently featured. OAuth client connections Object Height Offender Old link found for "%s"; removing.
 Old password Older → Oops! Oops, your comment was empty. OpenID OpenID was successfully removed. OpenID's Operation not allowed Or login with OpenID! Or login with Persona! Or login with a password! Or register with OpenID! Or register with Persona! Original Original file PDF file Password Path to the csv file containing metadata information. Persona's Perspective Please check your entries and try again. Post new media as you Powered by <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, a <a href="http://gnu.org/">GNU</a> project. Primary Privilege Profile changes saved Promote RESOLVED Really delete %(title)s? Really delete collection: %(title)s? Really delete user '%(user_name)s' and all related media/comments? Really remove %(media_title)s from %(collection_title)s? Reason Reason for Reporting Recover password Redirect URI Released under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Source code</a> available. Remove Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Resend verification email Resent your verification email. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Save Save changes Secondary See your information (e.g profile, media, etc...) Send instructions Send the user a message Separate tags by commas. Set password Set your new password Side Sign in to create an account! Skipping "%s"; already set up.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Someone has registered an account with this username, but it still has to be activated. Sorry Dave, I can't let you do that!</p><p>You have tried  to perform a function that you are not allowed to. Have you been trying to delete all user accounts again? Sorry, I don't support that file type :( Sorry, a user with that email address already exists. Sorry, a user with that name already exists. Sorry, an account is already registered to that OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Sorry, authentication is disabled on this instance. Sorry, comments are disabled. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Sorry, registration is disabled on this instance. Sorry, reporting is disabled on this instance. Sorry, the OpenID server could not be found Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Sorry, this audio will not work because 
	your web browser does not support HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Sorry, this video will not work because
      your web browser does not support HTML5 
      video. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Stay logged in Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Tagged with Tags Take away privilege Terms of Service Tertiary That OpenID is not registered to this account. That Persona email address is not registered to this account. The Persona email address was successfully removed. The blog was not deleted because you have no rights. The client {0} has been registered! The collection was not deleted because you didn't check that you were sure. The item was not removed because you didn't check that you were sure. The media was not deleted because you didn't check that you were sure. The name of the OAuth client The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. The request sent to the server is invalid, please double check it The slug can't be empty The title can't be empty The title part of this collection's address. You usually don't need to change this. The title part of this media's address. You usually don't need to change this. The user id is incorrect. The verification key or user id is incorrect The verification key or user id is incorrect. There doesn't seem to be a page at this address. Sorry!</p><p>If you're sure the address is correct, maybe the page you're looking for has been moved or deleted. There doesn't seem to be any media here yet... These uploads failed to process: This address contains errors This field does not take email addresses. This field is required for public clients This field requires an email address. This is where your media will appear, but you don't seem to have added anything yet. This site is running <a href="http://mediagoblin.org">MediaGoblin</a>, an extraordinarily great piece of media hosting software. This user hasn't filled in their profile (yet). This will be visible to users allowing your
                application to authenticate as them. This will be your default license on upload forms. Title To add your own media, place comments, and more, you can log in with your MediaGoblin account. Top Type UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Username Username or Email Username or email Value Verification cancelled Verification of %s failed: %s Verify your email! Video transcoding failed View View all of %(username)s's media View most recent media View on <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL WebM file (VP8/Vorbis) WebM file (Vorbis codec) Website What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Woohoo! Submitted! Woohoo! edited blogpost is submitted Wrong password Yes Yes, really delete my account You added the attachment %s! You already have a collection called "%s"! You are Banned. You are about to delete an item from another user's collection. Proceed with caution. You are about to delete another user's Blog. Proceed with caution. You are about to delete another user's collection. Proceed with caution. You are about to delete another user's media. Proceed with caution. You are editing a user's profile. Proceed with caution. You are editing another user's collection. Proceed with caution. You are editing another user's media. Proceed with caution. You are logged in as You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! You can get a modern web browser that 
      can play this video at <a href="http://getfirefox.com">
      http://getfirefox.com</a>! You can now log in using your new password. You can only edit your own profile. You can track the state of media being processed for your gallery here. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> for formatting. You can use <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> for formatting. You can't delete your only OpenID URL unless you have a password set You can't delete your only Persona email address unless you have a password set. You cannot take action against an administrator You deleted the Blog. You deleted the collection "%s" You deleted the item from the collection. You deleted the media. You have been banned You have to select or add a collection You must be logged in so we know who to send the email to! You must provide a file. You need to confirm the deletion of your account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the You've already verified your email address! Your OAuth clients Your OpenID url was saved successfully. Your Persona email address was saved successfully. Your comment has been posted! Your email address has been verified. Your email address has been verified. You may now login, edit your profile, and submit images! Your last 10 successful uploads Your password was changed successfully an unknown application commented on your post day feature management panel. feed icon hour indefinitely log out minute month newer older unoconv failing to run, check log file until %(until_when)s week year {files_uploaded} out of {files_attempted} files successfully submitted ← Newer ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Browsing media by <a href="%(user_url)s">%(username)s</a> Project-Id-Version: GNU MediaGoblin
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-07-10 12:32-0500
PO-Revision-Date: 2014-07-10 17:32+0000
Last-Translator: cwebber <cwebber@dustycloud.org>
Language-Team: Polish (http://www.transifex.com/projects/p/mediagoblin/language/pl/)
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            ❖ Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.  na dostęp do twojego konta? "%s" dodano do kolekcji "%s" "%s" już obecne w kolekcji "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (kolekcja użytkownika %(username)s) %(collection_title)s użytkownika <a href="%(user_url)s">%(username)s</a> %(formatted_time)s temu %(username)s's Privileges kolekcja użytkownika %(username)s Media użytkownika %(username)s Profil użytkownika %(username)s (usuń) + -- wybierz -- -----------{display_type}-Features---------------------------
 konto <a href="%(user_url)s">%(user_name)s</a> kolekcje użytkownika <a href="%(user_url)s">%(username)s</a> media użytkownika <a href="%(user_url)s">%(username)s</a> pliki użytkownika <a href="%(user_url)s">%(username)s</a> z tagiem <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - Klient może wysyłać żądania
                do instancji GNU MediaGoblin, która nie może zostać
                przechwycona przez agenta (np. klient po stronie serwera).<br />
                <strong>Public</strong> -  Klient nie może wysyłać poufnych
                żądań do instakcji GNU MediaGoblin (np. skrypt JavaScript
                 po stronie klienta). Kolekcja tego użytkownika z takim slugiem już istnieje. Zapisano ustawienia konta Action Taken Active Reports Filed Active Reports on %(username)s Active Users Dodaj Add Blog Post Dodaj adres poczty elektronicznej powiązany z kontem Persona Dodaj kolekcję Dodaj komentarz Dodaj nową kolekcję Dodaj konto OpenID Dodaj załącznik Dodaj media Add new Row Dodaj komentarz Dodaj do kolekcji Dodaj swoje media Dodaj “%(media_title)s” do kolekcji Dodano All reports on %(username)s All reports that %(username)s has filed Wszystkie prawa zastrzeżone Zezwól Prawie gotowe! Twoje konto oczekuje na aktywację. An acceptable processing file was not found Wysłano e-mail z instrukcjami jak zmienić hasło. Za kilka chwil powinieneś otrzymać e-mail z instrukcjami jak to zrobić. Adres z tym slugiem dla tego użytkownika już istnieje. Wystąpił błąd Aplikacje z dostępem do twojego konta mają prawo do: Kanał Atom Załączniki Uwierzytelnianie Uwierzytelnianie zakończone. Uwierzytelnianie zakończone. Uwierzytelnij BANNED until %(expiration_date)s Niewłaściwe żądanie Ban User Ban the user Banned Indefinitely Biogram Przeglądaj kolekcje CAUTION: Ciasteczko CSFR nie jest dostępne. Najprawdopodobniej stosujesz jakąś formę blokowania ciasteczek.<br/>Upewnij się, że nasz serwer może zakładać ciasteczka w twojej przeglądarce. Anuluj Nie można podlinkować motywu... nie wybrano motywu
 Zmień ustawienia konta Zmień swoje dane Change your password. Zmiana ustawień konta %(username)s Zmieniam konto poczty elektronicznej dla %(username)s Zmieniam hasło użytkownika %(username)s Clear empty Rows Closed Reports Znajduje się w kolekcji  Kolekcja Kolekcja "%s" została dodana! Komentarz Podgląd komentarza Skopiuj i wklej to do swojego klienta: Kopiowanie do dysku publicznego nie powiodło się. Nie mogę zrobić odnośnika "%s": %s istnieje i nie jest odnośnikiem
 Nie udało się odczytać pliku grafiki. Nie udało się wysłać e-maila w celu odzyskania hasła, ponieważ twoje konto jest nieaktywne lub twój adres e-mail nie został zweryfikowany. Nie potrafię znaleźć nikogo o tej nazwie użytkownika. Utwórz Create a Blog Utwórz konto! Utwórz nową kolekcję Utwórz je tutaj! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Utworzono Usuń Delete Blog Usuń adres poczty elektronicznej powiązany z kontem Persona Usuń konto OpenID Delete collection %(collection_title)s Usuń moje konto Usuń na stałe Delete the content Demote Odrzuć Opis Description of Report Opis tej kolekcji Opis tej pracy Czy chcesz uwierzytelnić Nie masz jeszcze konta? Jeszcze go nie masz? To proste! Don't process eagerly, pass off to celery Pobierz Pobierz model Edytuj Edit Blog Edit Metadata Edytuj profil Edycja %(collection_title)s Edytowanie %(media_title)s Edycja profilu %(username)s Edycja załączników do %(media_title)s Adres poczty elektronicznej Adres e-mail Powiadamiaj mnie e-mailem o komentarzach do moich mediów Wymagana weryfikacja adresu e-mail. Włącz powiadomienia dotyczące wydarzeń Enter the URL for the media to be featured Wprowadź swoje stare hasło aby udowodnić, że to twoje konto. Wprowadź swoje hasło aby potwierdzić, że jesteś właścicielem konta. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Odkrywaj FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel Plik Format pliku File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Zapomniałeś hasła? Początek Idź do strony: Granted Tu możesz śledzić stan przetwarzania mediów na tym serwerze. W tym miejscu można się przedstawić innym. Cześć %(username)s,

aby aktywować twoje konto GNU MediaGoblin, otwórz następującą stronę w swojej przeglądarce:

%(verification_url)s Witaj %(username)s,
%(comment_author)s skomentował twój wpis (%(comment_url)s) na %(instance_name)s
 Cześć, witaj na stronie MediaGoblin! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? Znaleziono stary odnośnik symboliczny do katalogu; usunięto.
 Na pewno chcę to usunąć Na pewno chcę usunąć ten element z kolekcji ID Identifier Jeśli ten adres poczty elektronicznej istnieje (uwzględniając wielkość liter!), wysłano na niego list z instrukcją, w jaki sposób możesz zmienić swoje hasło. Jeśli jesteś tą osobą, ale zgubiłeś swój e-mail weryfikujący, to możesz się <a href="%(login_url)s">zalogować</a> i wysłać go ponownie. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Grafika dla %(media_title)s Grafika zestresowanego goblina Jeśli nie nadejdzie: Dodaj notatkę Nieprawidłowa nazwa konta albo niewłaściwy adres poczty elektronicznej. Niewłaściwy plik dla tego rodzaju mediów. Is there another way to manage featured media? Ostatnie 10 udanych wysyłek Licencja Ulubiona licencja Położenie Zaloguj się Zaloguj się, aby utworzyć konto! Wyloguj się Logowanie nie powiodło się! Oznacz wszystkie jako przeczytane Max file size: {0} mb Przetwarzane media Panel przetwarzania mediów Media ze znacznikami: %(tag_name)s Logo MediaGoblin MetaData Metadata Metadata for "%(media_name)s" Najnowsze media Musisz podać oauth_token. Nazwa Name of user these media entries belong to Nowe komentarze Nowy adres poczty elektronicznej Nowe hasło No Nie znaleziono serwisu OpenID dla %s No active reports filed on %(username)s Brak katalogu danych dla tego motywu
 No closed reports found. Brak nieprzetworzonych wpisów! Żadne media nie są obecnie przetwarzane No open reports found. Na razie nie przetworzono żadnego wpisu! Nie znaleziono żetonu żądania. No users found. Nothing is currently featured. Połączenia do OAuth Wysokość obiektu Offender Znaleziono stary odnośnik dla "%s"; usuwam.
 Stare hasło Starsze → Ups! Ups, twój komentarz nie zawierał treści. OpenID Konto OpenID zostało pomyślnie usunięte. OpenID Operacja niedozwolona Albo zaloguj się kontem OpenID! Lub zaloguj z kontem Persona! Albo zaloguj się hasłem! Lub zarejestruj się z OpenID! Lub zarejestruj z kontem Persona! Oryginał Oryginalny plik Plik PDF Hasło Path to the csv file containing metadata information. Persona's Perspektywa Sprawdź swoje wpisy i spróbuj ponownie. Opublikuj nowe media na swoim koncie Napędzane przez oprogramowanie <a href="http://mediagoblin.org/" title='w wersji  %(version)s'>MediaGoblin</a>, będące częścią projektu <a href="http://gnu.org/">GNU</a>. Primary Privilege Zapisano zmiany profilu Promote RESOLVED Na pewno usunąć %(title)s? Really delete collection: %(title)s? Czy naprawdę skasować użytkownika '%(user_name)s' oraz usunąć wszystkie jego pliki i komentarze? Na pewno usunąć %(media_title)s z %(collection_title)s? Reason Reason for Reporting Odtwórz hasło Przekierowanie URI Opublikowane na licencji <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. Dostępny jest <a href="%(source_link)s">kod źródłowy</a>. Usuń Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Wyślij ponownie e-mail weryfikujący Wyślij ponownie e-mail weryfikujący. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Zachowaj Zapisz zmiany Secondary Zobacz swoje dane (np. profil, media, itp.) Wyślij instrukcje Send the user a message Rozdzielaj znaczniki przecinkami. Podaj hasło Podaj swoje nowe hasło Krawędź Sign in to create an account! Opuszczam "%s"; już jest gotowe.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Ktoś zarejestrował konto o tej nazwie, ale nadal oczekuje ono na aktywację. Misiaczku, nie możesz tego uczynić!</p><p>Próbowałeś wykonać działanie, do którego nie masz uprawnień. Czy naprawdę chciałeś skasować znowu wszystkie konta? NIestety, nie obsługujemy tego typu plików :-( Niestety użytkownik z tym adresem e-mail już istnieje. Niestety użytkownik o takiej nazwie już istnieje. Przepraszamy, ale istnieje już konto zarejestrowane z tym OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Przepraszamy, autoryzacja jest wyłączona w tym systemie. Komentowanie jest wyłączone. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Niestety rejestracja w tym serwisie jest wyłączona. Sorry, reporting is disabled on this instance. Przepraszamy, serwer OpenID nie został znaleziony Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Niestety, ten plik dźwiękowy nie zostanie odtworzony, 
	ponieważ ta przeglądarka nie obsługuje znaczników  
	dźwięku w HTML5. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Niestety ten materiał nie będzie widoczny⏎, ponieważ twoja przeglądarka nie⏎ osbługuje formatu HTML5. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Pozostań zalogowany Zaprenumerowano komentarze do %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Znaczniki: Znaczniki Take away privilege Terms of Service Tertiary Wskazane OpenID nie jest powiązane z niniejszym kontem. Niniejsze konto nie jest powiązane z adresem poczty elektronicznej w Persona. Konto związane z Persona zostało pomyślnie usunięte. The blog was not deleted because you have no rights. Klient {0} został zarejestrowany! Ta kolekcja nie została usunięta, ponieważ nie zaznaczono, że jesteś pewien. Ten element nie został usunięty, ponieważ nie zaznaczono, że jesteś pewien. Media nie zostały usunięte ponieważ nie potwierdziłeś, że jesteś pewien. Nazwa klienta OAuth Przekierowanie URI dla aplikacji, to pole
            jest <strong>wymagane</strong> dla publicznych klientów. Żądanie wysłane do serwera jest nieprawidłowe, sprawdź je proszę ponownie Slug nie może być pusty Tytuł nie może być pusty Część adresu zawierająca tytuł. Zwykle nie musisz tego zmieniać. Fragment adresu mediów zawierający tytuł. Zwykle nie ma potrzeby aby go zmieniać. Identyfikator użytkownika nie jest prawidłowy. Nieprawidłowy klucz weryfikacji lub identyfikator użytkownika. Klucz kontrolny albo identyfikator użytkownika jest nieprawidłowy. Wygląda na to, że nic tutaj nie ma!</p><p>Jeśli jesteś pewny, że adres jest prawidłowy, być może strona została skasowana lub przeniesiona. Tu nie ma jeszcze żadnych mediów... NIe udało się przesłać tych plików: Ten adres zawiera błędy Niniejsze pole nie jest przeznaczone na adres poczty elektronicznej. To pole jest wymagane dla klientów publicznych Niniejsze pole wymaga podania adresu poczty elektronicznej. Tu będą widoczne twoje media, ale na razie niczego tu jeszcze nie ma. Ten serwis działa w oparciu o <a href="http://mediagoblin.org">MediaGoblin</a>, świetne oprogramowanie do publikowania mediów. Ten użytkownik nie wypełnił (jeszcze) opisu swojego profilu. To będzie widoczne dla użytkowników, pozwalając⏎ twojej aplikacji uwierzytelniać się jako oni. To będzie twoja domyślna licencja dla wgrywanych mediów. Tytuł Aby dodawać swoje pliki, komentować i wykonywać inne czynności, możesz się zalogować na swoje konto MediaGoblin. Góra Typ UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Użytkownik Nazwa konta lub adres poczty elektronicznej Użytkownik lub adres e-mail Value Potwierdzenie anulowane Potwierdzenie %s nie powiodło się: %s Zweryfikuj swój adres e-mail! Konwersja wideo nie powiodła się View Zobacz wszystkie media użytkownika %(username)s View most recent media Zobacz na <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL Plik WebM (VP8/Vorbis) plik WebM (kodek Vorbis) Strona internetowa What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Hura! Wysłano! Woohoo! edited blogpost is submitted Nieprawidłowe hasło Yes Tak, naprawdę chcę skasować swoje konto Dodałeś załącznik %s! Kolekcja "%s" już istnieje! You are Banned. Zamierzasz usunąć element z kolekcji innego użytkownika. Zachowaj ostrożność. You are about to delete another user's Blog. Proceed with caution. Zamierzasz usunąć kolekcję innego użytkownika. Zachowaj ostrożność. Za chwilę usuniesz media innego użytkownika. Zachowaj ostrożność. Edytujesz profil innego użytkownika. Zachowaj ostrożność. Edytujesz kolekcję innego użytkownika. Zachowaj ostrożność. Edytujesz media innego użytkownika. Zachowaj ostrożność. Jesteś zalogowany jako You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! Proszę pobrać przeglądarkę, która obsługuje  
	dźwięk w HTML5, pod adresem <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! Możesz pobrać porządną przeglądarkę, która jest w stanie odtworzyć ten materiał filmowy, ze strony <a href="http://getfirefox.com/">⏎ http://getfirefox.com</a>! Teraz możesz się zalogować używając nowego hasła. Masz możliwość edycji tylko własnego profilu. Tutaj możesz śledzić stan mediów przesyłanych do twojej galerii. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. Możesz formatować tekst za pomocą składni
<a ref="http://daringfireball.net/projects/markdown/basics" target="_blank">
Markdown</a>. Możesz formatować tekst za pomocą składni 
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a>. Możesz formatować tekst za pomocą składni <a href="http://daringfireball.net/projects/markdown/basics" target="_blank"> Markdown</a>. You can't delete your only OpenID URL unless you have a password set Nie możesz usunąć adresu poczty elektronicznej powiązanego z systemem Persona, gdyż twoje konto zostanie wtedy bez jakiegokolwiek hasła. You cannot take action against an administrator You deleted the Blog. Usunięto kolekcję "%s" Element został usunięty z kolekcji. Media zostały usunięte. You have been banned Musisz wybrać lub dodać kolekcję Musisz się zalogować żebyśmy wiedzieli do kogo wysłać e-mail! Musisz podać plik. Musisz potwierdzić, że chcesz skasować swoje konto. Nie będziesz otrzymywać komentarzy do %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the Twój adres e-mail już został zweryfikowany! Twoi klienci OAuth Twój OpenID url został pomyślnie zachowany. Twój adres poczty elektronicznej powiązany z Persona został zachowany pomyślnie. Twój komentarz został opublikowany! Twój adres poczty elektronicznej został potwierdzony. Twój adres e-mail został zweryfikowany. Możesz się teraz zalogować, wypełnić opis swojego profilu i wysyłać grafiki! Ostatnie 10 twoich udanych wysyłek Twoje hasło zostało zmienione nieznana aplikacja komentarze do twojego wpisu dzień feature management panel. ikona kanału godzina indefinitely wyloguj się minuta miesiąc nowsze starsze nie dało się uruchomić unoconv, sprawdź log until %(until_when)s tydzień rok {files_uploaded} out of {files_attempted} files successfully submitted ← Nowsze ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Przeglądanie mediów użytkownika <a href="%(user_url)s">%(username)s</a> 