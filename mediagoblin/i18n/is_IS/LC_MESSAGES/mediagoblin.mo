��    �     $              ,  =   -  ;   k  8   �  z   �  ?   [  �   �  o      A  �   L   �$    %  �  +'  {   )  �  �)  �   {+  H   m,  M   �,     -     -     %-     7-     I-     Z-     n-     �-     �-     �-     �-  0   �-  ?   '.     g.     ~.     �.     �.     �.     �.     �.     �.  >   �.  2   6/  5   i/  /   �/  Z   �/     *0     E0  �  \0  9   �1     2     52     B2     W2     v2     �2     �2     �2     �2     �2     �2     �2     �2  	   3     3     3     )3     =3  )   L3     v3     |3  '   �3     �3     �3  6   �3  +   4  H   =4  J   �4  5   �4     5  .   5  	   G5     Q5     ]5     k5     �5  	   �5      �5     �5     �5     �5     �5     �5     �5     6  �   6     �6  "   �6     �6     �6     7  (   $7     M7      k7     �7     �7     �7  
   �7     �7     �7     �7  %   �7  !   8  4   ;8     p8  z   �8  )   
9     49     ;9     I9     \9     r9     �9     �9     �9     �9     �9     �9     �9     :  &   :     ::     L:     _:     r:     y:     ~:     �:     �:     �:     �:     �:     ;  )   ,;     V;     _;     n;  	   s;     };     �;     �;     �;     �;  '   �;     <     <  (   (<     Q<  )   k<  *   �<  6   �<  2   �<  Z   *=     �=  S   �=  ?   �=  <   !>  4   ^>     �>     �>     �>     �>     �>  .   �>  �   ?     �?     �?     @     @  G   @  ,   a@  }   �@  b   A  +   oA  �   �A     �B  O   �B     �B  "   C  4   7C     lC  8   �C     �C  
   �C  ~   �C  y   RD     �D     LE     fE     �E     �E  #   �E  "   �E  .   �E     F     8F     @F     SF     \F     cF     �F     �F     �F     �F     �F     �F     �F     
G     G     $G     -G     KG     ]G     zG  *   G     �G     �G     �G     �G  "   �G  '   �G  "   $H     GH     `H     sH     �H     �H     �H     �H     �H     I     I     *I  #   3I     WI  	   dI     nI     tI     �I      �I     �I     �I     �I     �I     J      J     9J     SJ     \J     jJ     sJ  5   |J  	   �J     �J  (   �J     �J  �   K     �K  	   �K     �K     �K     �K     �K  $   �K  B   L  8   HL     �L     �L     �L     �L  �   �L     KM  0   RM     �M     �M  	   �M     �M     �M     �M     �M     �M     �M     N     #N     4N     KN     fN     �N     �N     �N     �N     �N     �N     �N     �N  	   O  1   O     AO     SO     kO     �O     �O     �O     �O     �O     �O  _   �O  W   OP  �   �P  (   MQ  5   vQ  ,   �Q  7   �Q  >   R  H   PR  3   �R     �R     �R     S  .   #S  1   RS  .   �S  +   �S      �S  X    T  Z   YT  k   �T  c    U  ?   �U  *   �U     �U     �U     V  �   #V     �V     �V     �V     �V     �V  .   �V  =   W  3   UW  4   �W  #   �W  K   �W  E   .X  F   tX     �X  n   �X  A   GY     �Y     �Y  S   �Y  N   Z     ]Z  ,   wZ  -   �Z  �   �Z  .   t[      �[     �[  )   �[  )   \  %   5\  T   [\  �   �\  /   1]  `   a]  2   �]     �]  ^   �]     Z^     ^^  
   c^  	   n^     x^     �^  
   �^     �^     �^     �^     �^     �^     _     
_     !_     ?_     R_     k_      p_     �_  /   �_     �_     �_     �_     `     `  0   #`  7   T`  #   �`     �`     �`     �`     �`  $   �`     !a     0a     4a     Ra  *   oa     �a  U   �a  B    b  H   Cb  C   �b  7   �b  @   c  ;   Ic     �c  ]   �c  {   �c  |   td  �   �d  �   e  +   f  #   1f  G   Uf  �   �f  �   4g  �   �g  u   jh  D   �h  P   %i  /   vi     �i     �i  )   �i     j     j  &   2j  :   Yj     �j  1   �j  6   �j  .  k  +   El     ql  '   �l  2   �l     �l  %   �l  ^   #m     �m  &   �m     �m     �m     �m     �m  	   n     n     $n     1n     9n     @n     Fn     Ln  &   Rn     yn     �n     �n  F   �n  	   �n  8   �n  =   "o  �  `o  E   -q  >   sq  >   �q  z   �q  ?   lr     �r  o   ,s  A  �s  V   �w    5x  �  Dz  {   0|  �  �|  �   �~  C   �  W   �     "�     3�     C�     U�     g�     x�  (   ��     ��  $   р     ��     
�  1   %�  I   W�     ��     ��     ԁ     �     �     '�     4�     6�  >   B�  :   ��  5   ��  3   �  f   &�     ��     ��  �  ǃ  K   k�     ��     ԅ     �     �      �  
   0�     ;�  !   I�     k�     {�     ��     ��     Æ     ؆     �     �     �     �  !   .�  
   P�     [�  '   w�     ��     ��  M   ��      �  j   ,�  q   ��  P   	�     Z�  8   h�     ��  	   ��     ��     Ɖ     ԉ     �      �     �     &�     /�     =�     Q�     Y�     g�  �   p�     3�  ,   ?�  !   l�     ��     ��  4   ȋ  -   ��  /   +�     [�     l�     ��     ��     ��  
   ��     Č  .   ،  7   �  7   ?�     w�  �   ��  /   +�     [�     d�     r�     ��     ��     ��     ю     �  
   �     �     �      �     =�  &   T�     {�     ��     ��     ��     ��     ��     Ǐ     ݏ     ��     �     �  7   ;�  )   s�     ��     ��     ��  	   ��     ɐ     א     �     �  )   �  (   F�     o�     w�  H   ��  $   Б  (   ��  *   �  N   I�  L   ��  Z   �     @�  S   G�  ?   ��  <   ۓ  4   �     M�     U�     n�     t�     ��  .   ��  �   ��      ��  	        ̕     ܕ  [   �  1   @�  �   r�  w   �  7   ~�  G  ��     ��  O   �     b�  "   x�  6   ��  )   ҙ  A   ��  	   >�  
   H�  �   S�  �   �     ��     �     +�     G�     ]�     v�  4   ��  .   ˜  +   ��     &�     4�     N�     [�  2   d�  	   ��     ��     ��     ҝ     �     �     �     9�     V�     _�     h�     ��  4   ��     Ȟ  *   Ξ     ��     �     �     +�  -   .�  '   \�  $   ��  &   ��     П     �  !   �     &�     B�     X�     p�     ��     ��     ��  -   ��     �  	   ��     �  .   �     @�  /   Q�     ��     ��  0   ��  -   ڡ  *   �  /   3�  ,   c�     ��     ��  	   ��  	   ��  5   Ǣ     ��  	   �  ;   �  )   U�  �   �     
�  	   �  "   �     ?�     G�     M�  $   h�  C   ��  H   Ѥ  	   �     $�     A�     W�  �   p�  
    �  0   �  
   <�     G�  	   a�     k�     �     ��     ��     ��     ��     Ϧ     �     ��  !   �     5�  $   T�     y�     �     ��  "   ��     ��     ק     ݧ  	   �  L   ��     E�     Z�  "   u�     ��  &   ��     Ϩ  +   ը  3   �     5�  _   F�  `   ��  �   �  5   ժ  J   �  8   V�  S   ��  X   �  X   <�  ?   ��  (   լ  +   ��     *�  .   E�  ;   t�  6   ��  -   �  !   �  X   7�  i   ��  k   ��  l   f�  K   ӯ  -   �     M�     T�  3   d�  �   ��  
   �  	   )�     3�     G�     Y�  ;   b�  C   ��  5   �  4   �  &   M�  X   t�  X   Ͳ  J   &�     q�     ��  T   �  ,   \�  (   ��  V   ��  T   	�     ^�  8   {�  9   ��  �   �  5   ��  3   ��      (�  )   I�  6   s�  5   ��  e   �  �   F�  E   ܸ  r   "�  D   ��     ڹ  x   �     Z�     a�  
   h�  	   s�     }�     ��     ��     ��     ̺     ߺ     �     
�     #�     )�  -   B�     p�  "   ��     ��  !   ��     ׻  1   �      �     0�     6�  "   N�  	   q�  :   {�  7   ��  )   �     �     %�  )   6�  '   `�  $   ��     ��     ��  0   ��  !   �  .   �     C�  `   V�  B   ��  W   ��  Z   R�  =   ��  A   �  >   -�     l�  o   u�  {   ��  �   a�  �   ��  �   v�  @   	�  /   J�  d   z�  �   ��  �   ~�  �   2�  �   ��  P   ]�  \   ��  0   �     <�     R�  &   m�     ��     ��  +   ��  W   ��      P�  C   q�  I   ��  .  ��  &   .�     U�  2   p�  6   ��     ��  (   ��  x   #�  2   ��  +   ��     ��  *   �     8�     >�     X�     j�     v�     ��     ��  	   ��     ��     ��  4   ��     ��     ��     �  F   �  
   L�  8   W�  G   ��   
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            ❖ Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   to access your account?  "%s" added to collection "%s" "%s" already in collection "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (%(username)s's collection) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s ago %(username)s's Privileges %(username)s's collections %(username)s's media %(username)s's profile (remove) + -- Select -- -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a>'s account <a href="%(user_url)s">%(username)s</a>'s collections <a href="%(user_url)s">%(username)s</a>'s media <a href="%(user_url)s">%(username)s</a>'s media with tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). A collection with that slug already exists for this user. Account settings saved Action Taken Active Reports Filed Active Reports on %(username)s Active Users Add Add Blog Post Add a Persona email address Add a collection Add a comment Add a new collection Add an OpenID Add attachment Add media Add new Row Add this comment Add to a collection Add your media Add “%(media_title)s” to a collection Added All reports on %(username)s All reports that %(username)s has filed All rights reserved Allow Almost done! Your account still needs to be activated. An acceptable processing file was not found An email has been sent with instructions on how to change your password. An email should arrive in a few moments with instructions on how to do so. An entry with that slug already exists for this user. An error occured Applications with access to your account can:  Atom feed Attachments Authorization Authorization Complete Authorization Finished Authorize BANNED until %(expiration_date)s Bad Request Ban User Ban the user Banned Indefinitely Bio Browse collections CAUTION: CSRF cookie not present. This is most likely the result of a cookie blocker or somesuch.<br/>Make sure to permit the settings of cookies for this domain. Cancel Cannot link theme... no theme set
 Change account settings Change your information Change your password. Changing %(username)s's account settings Changing %(username)s's email Changing %(username)s's password Clear empty Rows Closed Reports Collected in Collection Collection "%s" added! Comment Comment Preview Copy and paste this into your client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Could not read the image file. Could not send password recovery email as your username is inactive or your account's email address has not been verified. Couldn't find someone with that username. Create Create a Blog Create an account! Create new collection Create one here! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Created Delete Delete Blog Delete a Persona email address Delete an OpenID Delete collection %(collection_title)s Delete my account Delete permanently Delete the content Demote Deny Description Description of Report Description of this collection Description of this work Do you want to authorize  Don't have an account yet? Don't have one yet? It's easy! Don't process eagerly, pass off to celery Download Download model Edit Edit Blog Edit Metadata Edit profile Editing %(collection_title)s Editing %(media_title)s Editing %(username)s's profile Editing attachments for %(media_title)s Email Email address Email me when others comment on my media Email verification needed Enable insite notifications about events. Enter the URL for the media to be featured Enter your old password to prove you own this account. Enter your password to prove you own this account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Explore FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File File Format File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Forgot your password? Front Go to page: Granted Here you can track the state of media being processed on this instance. Here's a spot to tell others about yourself. Hi %(username)s,

to activate your GNU MediaGoblin account, open the following URL in
your web browser:

%(verification_url)s Hi %(username)s,
%(comment_author)s commented on your post (%(comment_url)s) at %(instance_name)s
 Hi there, welcome to this MediaGoblin site! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 I am sure I want to delete this I am sure I want to remove this item from the collection ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. If you are that person but you've lost your verification email, you can <a href="%(login_url)s">log in</a> and resend it. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out In case it doesn't: Include a note Invalid User name or email address. Invalid file given for media type. Is there another way to manage featured media? Last 10 successful uploads License License preference Location Log in Log in to create an account! Log out Logging in failed! Mark all read Max file size: {0} mb Media in-processing Media processing panel Media tagged with: %(tag_name)s MediaGoblin logo MetaData Metadata Metadata for "%(media_name)s" Most recent media Must provide an oauth_token. Name Name of user these media entries belong to New comments New email address New password No No OpenID service was found for %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. No failed entries! No media in-processing No open reports found. No processed entries, yet! No request token found. No users found. Nothing is currently featured. OAuth client connections Object Height Offender Old link found for "%s"; removing.
 Old password Older → Oops! Oops, your comment was empty. OpenID OpenID was successfully removed. OpenID's Operation not allowed Or login with OpenID! Or login with Persona! Or login with a password! Or register with OpenID! Or register with Persona! Original Original file PDF file Password Path to the csv file containing metadata information. Persona's Perspective Please check your entries and try again. Post new media as you Powered by <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, a <a href="http://gnu.org/">GNU</a> project. Primary Privilege Profile changes saved Promote RESOLVED Really delete %(title)s? Really delete collection: %(title)s? Really delete user '%(user_name)s' and all related media/comments? Really remove %(media_title)s from %(collection_title)s? Reason Reason for Reporting Recover password Redirect URI Released under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Source code</a> available. Remove Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Resend verification email Resent your verification email. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Save Save changes Secondary See your information (e.g profile, media, etc...) Send instructions Send the user a message Separate tags by commas. Set password Set your new password Side Sign in to create an account! Skipping "%s"; already set up.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Someone has registered an account with this username, but it still has to be activated. Sorry Dave, I can't let you do that!</p><p>You have tried  to perform a function that you are not allowed to. Have you been trying to delete all user accounts again? Sorry, I don't support that file type :( Sorry, a user with that email address already exists. Sorry, a user with that name already exists. Sorry, an account is already registered to that OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Sorry, authentication is disabled on this instance. Sorry, comments are disabled. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Sorry, registration is disabled on this instance. Sorry, reporting is disabled on this instance. Sorry, the OpenID server could not be found Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Sorry, this audio will not work because 
	your web browser does not support HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Sorry, this video will not work because
      your web browser does not support HTML5 
      video. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Stay logged in Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Tagged with Tags Take away privilege Terms of Service Tertiary That OpenID is not registered to this account. That Persona email address is not registered to this account. The Persona email address was successfully removed. The blog was not deleted because you have no rights. The client {0} has been registered! The collection was not deleted because you didn't check that you were sure. The item was not removed because you didn't check that you were sure. The media was not deleted because you didn't check that you were sure. The name of the OAuth client The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. The request sent to the server is invalid, please double check it The slug can't be empty The title can't be empty The title part of this collection's address. You usually don't need to change this. The title part of this media's address. You usually don't need to change this. The user id is incorrect. The verification key or user id is incorrect The verification key or user id is incorrect. There doesn't seem to be a page at this address. Sorry!</p><p>If you're sure the address is correct, maybe the page you're looking for has been moved or deleted. There doesn't seem to be any media here yet... These uploads failed to process: This address contains errors This field does not take email addresses. This field is required for public clients This field requires an email address. This is where your media will appear, but you don't seem to have added anything yet. This site is running <a href="http://mediagoblin.org">MediaGoblin</a>, an extraordinarily great piece of media hosting software. This user hasn't filled in their profile (yet). This will be visible to users allowing your
                application to authenticate as them. This will be your default license on upload forms. Title To add your own media, place comments, and more, you can log in with your MediaGoblin account. Top Type UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Username Username or Email Username or email Value Verification cancelled Verification of %s failed: %s Verify your email! Video transcoding failed View View all of %(username)s's media View most recent media View on <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL WebM file (VP8/Vorbis) WebM file (Vorbis codec) Website What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Woohoo! Submitted! Woohoo! edited blogpost is submitted Wrong password Yes Yes, really delete my account You added the attachment %s! You already have a collection called "%s"! You are Banned. You are about to delete an item from another user's collection. Proceed with caution. You are about to delete another user's Blog. Proceed with caution. You are about to delete another user's collection. Proceed with caution. You are about to delete another user's media. Proceed with caution. You are editing a user's profile. Proceed with caution. You are editing another user's collection. Proceed with caution. You are editing another user's media. Proceed with caution. You are logged in as You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! You can get a modern web browser that 
      can play this video at <a href="http://getfirefox.com">
      http://getfirefox.com</a>! You can now log in using your new password. You can only edit your own profile. You can track the state of media being processed for your gallery here. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> for formatting. You can use <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> for formatting. You can't delete your only OpenID URL unless you have a password set You can't delete your only Persona email address unless you have a password set. You cannot take action against an administrator You deleted the Blog. You deleted the collection "%s" You deleted the item from the collection. You deleted the media. You have been banned You have to select or add a collection You must be logged in so we know who to send the email to! You must provide a file. You need to confirm the deletion of your account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the You've already verified your email address! Your OAuth clients Your OpenID url was saved successfully. Your Persona email address was saved successfully. Your comment has been posted! Your email address has been verified. Your email address has been verified. You may now login, edit your profile, and submit images! Your last 10 successful uploads Your password was changed successfully an unknown application commented on your post day feature management panel. feed icon hour indefinitely log out minute month newer older unoconv failing to run, check log file until %(until_when)s week year {files_uploaded} out of {files_attempted} files successfully submitted ← Newer ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Browsing media by <a href="%(user_url)s">%(username)s</a> Project-Id-Version: GNU MediaGoblin
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-07-10 12:32-0500
PO-Revision-Date: 2014-07-10 17:32+0000
Last-Translator: cwebber <cwebber@dustycloud.org>
Language-Team: Icelandic (Iceland) (http://www.transifex.com/projects/p/mediagoblin/language/is_IS/)
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 
                Athugasemdartilkynning #%(report_id)s
               
                Efnistilkynning #%(report_id)s
               
              Kláruð tilkynning #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            ❖ Höfundur: <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          EFNI SEM
            <a href="%(user_url)s"> %(user_name)s</a>
          SETTI INN VAR EYTT
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Tilkynnt efni sem <a href="%(user_url)s">%(user_name)s</a> setti inn
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Hér getur þú flett upp opnum tilkynningum frá notendum.
   
    Hér getur þú skoðað notendur til að gera refsigaðgerðir gagnvart þeim.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.  að fá aðgang að aðganginum þínum? "%s" sett í albúmið "%s" "%s" er nú þegar í albúminu "%s" Fjöldi athugasemda %(blog_owner_name)s's Blog %(collection_title)s (albúm sem %(username)s á) %(collection_title)s sem <a href="%(user_url)s">%(username)s</a> bjó til Fyrir %(formatted_time)s %(username)s's Privileges Albúm sem %(username)s á Efni sem %(username)s á Kenniskrá fyrir: %(username)s (fjarlægja) + -- Velja -- -----------{display_type}-Features---------------------------
 Notandaaðgangur: <a href="%(user_url)s">%(user_name)s</a> Albúm sem <a href="%(user_url)s">%(username)s</a> á Efni sem <a href="%(user_url)s">%(username)s</a> á Efni sem <a href="%(user_url)s">%(username)s</a> á og er merkt með <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>Senda inn tilkynningu</h2> <strong>Trúnaður</strong> - Biðlarinn getur
                sent beiðnir til GNU MediaGoblin vefsvæðisins sem geta ekki verið
                truflaðar af notandaforriti (t.d. forriti á vefþjóni).<br />
                <strong>Opinbert</strong> - Biðlarinn getur ekki gert trúnaðarbundnar
                beiðnir til GNU MediaGoblin vefsvæðisins (t.d. Javascript biðlara
                hjá notanda). Albúm með þessu vefslóðarormi er nú þegar til fyrir þennan notanda. Aðgangsstillingar vistaðar Aðgerð tekin Virkar innsendar tilkynningar Active Reports on %(username)s Virkir notendur Bæta við Add Blog Post Bæta við „Persona“ netfangi Búa til albúm Bæta við athugasemd Búa til nýtt albúm Bæta við OpenID auðkenni Bæta við viðhengi Senda inn efni Add new Row Senda inn þessa athugasemd Setja í albúm Sendu inn efni Setja '%(media_title)s' í albúm Bætt við All reports on %(username)s All reports that %(username)s has filed Öll réttindi áskilin Leyfa Næstum því búið! Notandaaðgangurinn þinn verður að vera staðfestur. Nothæf keyrsluskrá fannst ekki Tölvupóstur hefur verið sendur með leiðbeiningum um hvernig þú átt að breyta lykilorðinu þínu. Tölvupóstur ætti að berast til þín eftir smástund með leiðbeiningum um hvernig þú átt að gera það. Efni merkt með þessum vefslóðarormi er nú þegar til fyrir þennan notanda. Villa kom upp Forrit með aðgang að notendaaðganginum þínum geta: Atom fréttaveita Viðhengi Heimilun Heimild klár Heimilun lokið Heimila BANNED until %(expiration_date)s Ekki nógu góð beiðni Ban User Banna notanda Banned Indefinitely Lýsing Skoða albúm CAUTION: CSRF smákaka ekki til staðar. Þetta er líklegast orsakað af smákökugildru eða einhverju þess háttar.<br/>Athugaðu hvort þú leyfir ekki alveg örugglega smákökur fyrir þetta lén. Hætta við Get ekki tengt þema... ekkert þema stillt
 Breyta stillingum notandaaðgangs Breytt upplýsingunum þínum Breyta lykilorðinu þínu. Breyti notandaaðgangsstillingum fyrir: %(username)s Breyti netfangi fyrir notandann: %(username)s Breyti lykilorði fyrir notandann: %(username)s Clear empty Rows Kláraðar tilkynningar Sett í albúm Albúm Albúmið "%s" var búið til! Athugasemd Útlit athugasemdar Afritaðu og límdu þetta í forritið þitt: Það tókst ekki að afrita yfir í almennu geymsluna. Gat ekki tengt "%s": %s er til og er ekki tákntengill
 Gat ekki lesið myndskrána. Gat ekki sent tölvupóst um endurstillingu lykilorðs því notandanafnið þitt er óvirkt eða þá að þú hefur ekki staðfest netfangið þitt. Gat ekki fundið neinn með þetta notandanafn. Búa til Create a Blog Búðu til nýjan aðgang! Búa til nýtt albúm Búðu til aðgang hérna! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Búið til Eyða Delete Blog Eyða „Persona“ netfangi Eyða OpenID auðkenni Delete collection %(collection_title)s Eyða aðganginum mínum Eytt algjörlega Eyða efninu Demote Banna Lýsing Description of Report Lýsing á þessu albúmi Lýsing á þessu efni Viltu heimila Ertu ekki með notendaaðgang? Ertu ekki með aðgang? Það er auðvelt að búa til! Don't process eagerly, pass off to celery Sækja Hala niður líkani Breyta Edit Blog Edit Metadata Breyta kenniskrá Breyti %(collection_title)s Breyti %(media_title)s Breyti kenniskrá notandans: %(username)s Breyti viðhengjum við: %(media_title)s Netfang Tölvupóstfang Senda mér tölvupóst þegar einhver bætir athugasemd við efnið mitt Staðfesting á netfangi nauðsynleg Virkja innri tilkynningar um viðburði. Enter the URL for the media to be featured Skráðu gamla lykilorðið þitt til að sanna að þú átt þennan aðgang. Sláðu inn lykilorðið þitt til að sanna að þú eigir þennan aðgang. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Skoða FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel Skrá Skráarsnið Tilkynna File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Gleymdirðu lykilorðinu þínu? Framhlið Fara á síðu: Granted Hér getur þú fylgst með margmiðlunarefni sem verið er að vinna á þessu vefsvæði. Hér er svæði til að segja öðrum frá þér. Hæ %(username)s,

til að virkja GNU MediaGoblin aðganginn þinn, opnaðu þá eftirfarandi vefslóði í
vafranum þínum:

%(verification_url)s Hæ %(username)s,
%(comment_author)s skrifaði athugasemd við færsluna þína (%(comment_url)s) á %(instance_name)s
 Hæ! Gakktu í bæinn á þetta MediaGoblin vefsvæði! Hi,

Við vildum staðfesta að þú værir notandinn „%(username)s“. Ef það er rétt,
vinsamlegast smelltu á tengilinn hér fyrir neðan til að staðfesta nýja netfangið þitt.

%(verification_url)s

Ef þú ert ekki „%(username)s“ or baðst ekki um að breytan netfanginu getur þú hunsað
þennan tölvupóst. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? Fann samt gamlan tákntengil á möppu; fjarlægður.
 Ég er viss um að ég vilji eyða þessu Ég er viss um að ég vilji fjarlægja þetta efni úr albúminu Auðkenni Identifier Ef þetta netfang (há- og lágstafir skipta máli) er skráð hjá okkur hefur tölvupóstur verið sendur með leiðbeiningum um hvernig þú getur breytt lykilorðinu þínu. Ef þú ert þessi aðili en hefur týnt staðfestingarpóstinum getur þú <a href="%(login_url)s">skráð þig inn</a> og endursent hann If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Mynd fyrir %(media_title)s Mynd af durt í stresskasti Ef það gerist ekki: Bæta við minnispunktum Ógilt notandanafn eða netfang Ógild skrá gefin fyrir þessa margmiðlunartegund. Is there another way to manage featured media? Síðustu 10 árangursríku innsendingarnar Notkunarleyfi Stilling á notkunarleyfi Staðsetning Innskrá Skráðu þig inn til að búa til nýjan aðgang! Skrá út Mistókst að skrá þig inn. Merkja allt lesið Hámarksskráarstærð: {0} MB Efni í vinnslu Margmiðlunarvinnsluskiki Efni merkt með: %(tag_name)s MediaGoblin einkennismerkið MetaData Metadata Metadata for "%(media_name)s" Nýlegt efni Þú verður að gefa upp OAuth tóka (oauth_token). Heiti Name of user these media entries belong to Nýjar athugasemdir Nýtt netfang Nýtt lykilorð No Engin OpenID þjónusta fannst fyrir „%s“ No active reports filed on %(username)s Engin eignamappa fyrir þetta þema
 Engar kláraðar tilkynningar fundust. Engar bilaðar innsendingar! Ekkert efni í vinnslu Engar opnar tilkynningar fundust. Ekkert fullunnið efni enn! Engin beiðni fannst. Engir notendur fundust. Nothing is currently featured. Biðlarartengingar OAuth Hæð hlutar Gerandi Gamall tengill fannst fyrir "%s"; fjarlægi.
 Gamla lykilorðið Eldri → Obbosí! Obbosí! Athugasemdin þín var innihaldslaus. OpenID auðkenni Það tókst að fjarlægja OpenID auðkennið. OpenID auðkenni Aðgerð ekki leyfileg ...eða skráðu þig inn með OpenID auðkenni! ...eða skráðu þig inn með „Persona“! ...eða skráðu þig inn með lykilorði! ...eða nýskráðu þig með OpenID auðkenni! ...eða nýskráðu þig með „Persona“! Upphafleg skrá Upphaflega skráin PDF skrá Lykilorð Path to the csv file containing metadata information. Persona auðkenni Fjarvídd Vinsamlegast kíktu á færslurnar þínar og reyndu aftur. Sent inn nýtt margmiðlunarefni sem þú Keyrt af <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, sem er <a href="http://gnu.org/">GNU</a> verkefni. Primary Privilege Breytingar á kenniskrá vistaðar Promote LEYST Virkilega eyða %(title)s? Really delete collection: %(title)s? Virkilega eyða notanda '%(user_name)s' og tengt efni/athugasemdir? Virkilega fjarlægja %(media_title)s úr %(collection_title)s albúminu? Ástæða Ástæður fyrir tilkynningu Endurstilla lykilorð Áframsendingarvefslóð Gefið út undir <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Frumkóti</a> aðgengilegur. Fjarlægja Remove %(media_title)s from %(collection_title)s Tilkynning Report #%(report_number)s Report ID Tilkynningastýring Tilkynna efni Tilkynningastýring Tilkynnt af Reported Comment Reported Content Reported Media Entry Tilkynnt athugasemd Tilkynni þessa athugasemd Tilkynni þetta margmiðlunarefni Endursenda staðfestingarpóst Endursendi staðfestingartölvupóst Leysa Leysa þessa tilkynningu Leyst Fara aftur í tilkynningastýringu Return to Users Panel Vista Vista breytingar Secondary Skoða upplýsingarnar þínar (t.d. kenniskrá, margmiðlunarefni o.s.frv.) Senda leiðbeiningar Senda notandanum skilaboð Aðskildu efnisorðin með kommum. Skrá lykilorð Skrifaðu inn nýja lykilorðið þitt Hlið Skráðu þig inn til að búa til aðgang! Hoppa yfir "%s"; hefur nú þegar verið sett upp.
 Vefslóðarormur Someone has registered an account with this username, but it still has
        to be activated. Einhver hefur búið til aðgang með þessu notandanafni en hefur ekki enn virkjað aðganginn. Fyrirgefðu Davíð. Ég get ekki leyft þér að gera þetta!</p></p>Þú reyndir að framkvæma aðgerð sem þú hefur ekki leyfi til að gera. Varstu að reyna að eyða öllum notendareikningum aftur? Ég styð því miður ekki þessa gerð af skrám :( Því miður þá er annar notandi í kerfinu með þetta netfang skráð. Því miður er nú þegar til notandi með þetta nafn. Því miður er annar aðgangur nú þegar skráður fyrir þetta OpenID auðkenni. Því miður er annar aðgangur nú þegar skráður fyrir þetta „Persona“ netfang. Því miður er annar aðgangur nú þegar skráður fyrir þetta „Persona“ netfang. Því miður er auðkenning ekki möguleg á þessu vefsvæði. Því miður, athugasemdir eru óvirkar. Því miður fannst engin slík tilkynning. Sorry, no such user found. Sorry, no user by username '{username}' exists Því miður er nýskráning ekki leyfð á þessu svæði. Því miður eru tilkynningar óvirkar á þessum vef. Því miður fannst OpenID netþjónninn ekki Því miður er skráin of stór. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Því miður mun þessi hljóðskrá ekki virka því 
	vafrinn þinn styður ekki HTML5 
	hljóðskrár. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Því miður mun þetta myndskeið ekki virka því
      vafrinn þinn styður ekki HTML5 
      myndbönd. Því miður mun upphal á þessari skrá sprengja upphalshámarkið þitt. Því miður hefur þú náð upphalshámarki Staða Muna eftir mér Þú ert nú áskrifandi að athugasemdum „%s“! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Merkt með Efnisorð Taka burt réttindi Notendaskilmálar Tertiary Þetta OpenID auðkenni er ekki skráð á þennan aðgang. Þetta „Persona“ netfang er ekki skráð fyrir þennan aðgang. Það tókst að fjarlægja „Persona“ netfangið. The blog was not deleted because you have no rights. Biðlarinn {0} hefur verið skráður! Þessu albúmi var ekki eytt vegna þess að þu merktir ekki við að þú værir viss. Þetta efni var ekki fjarlægt af því að þú merktir ekki við að þú værir viss. Efninu var ekki eytt þar sem þú merktir ekki við að þú værir viss. Nafn OAuth biðlarans Áframsendingarvefslóðin fyrir forritin, þessi reitur
            er <strong>nauðsynlegur</strong> fyrir opinbera biðlara. Beiðnin sem var send til netþjónsins er ógild, vinsamlegast athugaðu hana aftur Vefslóðarormurinn getur ekki verið tómur Þessi titill getur verið innihaldslaus Titilhlutinn í vefslóð þessa albúms. Þú þarft vanalega ekki að breyta þessu. Titilhlutinn í vefslóð þessa efnis. Þú þarft vanalega ekki að breyta þessu. Notendaauðkennið er rangt. Staðfestingarlykillinn eða notendaauðkennið er rangt Staðfestingarlykillinn eða notendaauðkennið er rangt. Því miður! Það virðist ekki vera nein síða á þessari vefslóð.</p><p>Ef þú ert viss um að vefslóðin sé rétt hefur vefsíðan sem þú ert að leita að kannski verið flutt eða fjarlægð. Það virðist ekki vera neitt efni hérna ennþá... Það mistókst að fullvinna þessar innsendingar: Þetta netfang inniheldur villur Þessi reitur tekur ekki við netföngum. Þessi reitur er nauðsynlegur fyrir opinbera biðlara í þennan reit verður að slá inn tölvupóstfang. Þetta er staðurinn þar sem efnið þitt birtist en þú virðist ekki hafa sent neitt inn ennþá. Þetta vefsvæði keyrir á <a href="http://mediagoblin.org">MediaGoblin</a> sem er ótrúlega frábær hugbúnaður til að geyma margmiðlunarefni. Þessi notandi hefur ekki fyllt inn í upplýsingar um sig (ennþá). Þetta verður sýnilegt öðrum notendum sem leyfir
                forritinu þínu að skrá sig inn sem þeir. Þetta verður sjálfgefna leyfið þegar þú vilt hlaða upp efni. Titill Til að senda inn þitt efni, gera athugasemdir og fleira getur þú skráð þig inn með þínum MediaGoblin aðgangi. Toppur Tegund UnBan User Unfeature Update Metadata Notendastýring Notendastýring Notandi í banni þangað til: User: %(username)s Notandanafn Notandanafn eða tölvupóstur Notandanafn eða netfang Value Hætt við staðfestingu Staðfesting á „%s“ mistóksts: „%s“ Staðfestu netfangið þitt! Þverkóðun myndskeiðs mistókst View Skoða efnið sem %(username)s á View most recent media Skoða á <a href="%(osm_url)s">OpenStreetMap</a> Viðvörun frá WebGL WebM skrá (VP8/Vorbis) WebM skrá (Vorbis víxlþjöppun) Vefsíða Hvað ætlarðu að gera til að vinna úr tilkynningunni? What is a Primary Feature? What is a Secondary Feature? Hvaða réttindi ætlarðu að taka burt? Skráði sig Hvenær tilkynnt Af hverju ertu að banna þennan notanda? Jibbí jei! Það tókst að senda inn! Woohoo! edited blogpost is submitted Rangt lykilorð Yes Já, ég vil örugglega eyða aðganginum mínum Þú bættir við viðhenginu %s! Þú hefur nú þegar albúm sem kallast "%s"! Þú ert í banni. Þú ert í þann mund að fara að eyða efni úr albúmi annars notanda. Farðu mjög varlega. You are about to delete another user's Blog. Proceed with caution. Þú ert í þann mund að fara að eyða albúmi annars notanda. Farðu mjög varlega. Þú ert í þann mund að fara að eyða efni frá öðrum notanda. Farðu mjög varlega. Þú ert að breyta kenniskrá notanda. Farðu mjög varlega. Þú ert að breyta albúmi annars notanda. Farðu mjög varlega. Þú ert að breyta efni annars notanda. Farðu mjög varlega. Þú ert Þú ert ekki lengur virkur notandi. Vinsamlegast talaðu við stjóranda til að endurvirkja aðganginn þinn. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! Þú getur náð í nýlegan vafra sem 
	getur spilað hljóðskrár á <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! Þú getur náð í nýlegan vafra sem 
      sem getur spilað myndskeiðið á <a href="http://getfirefox.com">
      http://getfirefox.com</a>! Þú getur núna innskráð þig með nýja lykilorðinu þínu. Þú getur bara breytt þinni eigin kenniskrá. Þú getur fylgst með hvernig gengur að vinna með margmiðlunarefnið fyrir safnið þitt hérna. Þú getur notað
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> fyrir stílsnið. Þú getur notað
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> til að stílgera textann. Þú getur notað
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> til að stílgera textann. Þú getur notað <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> til að stílgera textann. Þú getur ekki eytt einu OpenID vefslóðinni nema þú hafir skráð lykilorð Þú getur ekki eytt eina „Persona“ netfanginu þínu nema þú hafir skráð lykilorð. Þú getur ekki gert þetta gagnvart stjórnanda You deleted the Blog. Þú eyddir albúminu "%s" Þú tókst þetta efni úr albúminu. Þú eyddir þessu efni. Stjórnandi hefur bannað þig Þú verður að velja eða búa til albúm Þú verður að hafa innskráð þig svo við vitum hvert á að senda tölvupóstinn! Þú verður að gefa upp skrá. Þú verður að samþykkja eyðingu á notandaaðganginum þínum. Þú færð tilkynningar þegar einhver skrifar athugasemd við „%s“. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the Þú hefur staðfest netfangið þitt! OAuth-biðlararnir þínir Það tókst að vista OpenID vefslóðina þína. Það tókst að vista „Persona“ netfangið þitt. Athugasemdin þín var skráð! Netfangið þitt hefur verið staðfest. Netfangið þitt hefur verið staðfest. Þú getur núna innskráð þig, breytt kenniskránni þinni og sent inn efni! Síðustu 10 árangursíku innsendingarnar þínar Það tókst að breyta lykilorðinu þínu óþekktu forriti skrifaði athugasemd við færsluna þína dagur feature management panel. fréttaveituteikn klukkustund ótilgreint útskrá mínúta mánuður nýrri eldri tekst ekki að keyra unoconv, athugaðu annálsskrá þangað til %(until_when)s vika ár {files_uploaded} out of {files_attempted} files successfully submitted ← Nýrri ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Skoða efnið sem <a href="%(user_url)s">%(username)s</a> setti inn 