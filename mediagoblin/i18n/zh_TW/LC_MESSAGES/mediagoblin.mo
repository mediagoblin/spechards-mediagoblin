Þ    á     $              ,  =   -  ;   k  8   §  z   à  ?   [       o      A     L   Ï$    %  ë  +'  {   )  ç  )  ñ   {+  H   m,  M   ¶,     -     -     %-     7-     I-     Z-     n-     -     ¦-     Æ-     Û-  0   ö-  ?   '.     g.     ~.     .     ³.     È.     ß.     è.     ê.  >   ÷.  2   6/  5   i/  /   /  Z   Ï/     *0     E0    \0  9   ä1     2     52     B2     W2     v2     2     2     2     ±2     Â2     Ð2     å2     ó2  	   3     3     3     )3     =3  )   L3     v3     |3  '   3     À3     Ô3  6   Ú3  +   4  H   =4  J   4  5   Ñ4     5  .   5  	   G5     Q5     ]5     k5     5  	   5      £5     Ä5     Ð5     Ù5     æ5     ú5     þ5     6     6     ´6  "   »6     Þ6     ö6     7  (   $7     M7      k7     7     7     ¬7  
   ¹7     Ä7     Û7     ã7  %   ó7  !   8  4   ;8     p8  z   8  )   
9     49     ;9     I9     \9     r9     9     9     ¯9     È9     Ð9     ×9     ã9     :  &   :     ::     L:     _:     r:     y:     ~:     :      :     ¿:     Ø:     ò:     ;  )   ,;     V;     _;     n;  	   s;     };     ;     ;     µ;     Í;  '   ì;     <     <  (   (<     Q<  )   k<  *   <  6   À<  2   ÷<  Z   *=     =  S   =  ?   á=  <   !>  4   ^>     >     >     ´>     ¹>     Å>  .   Ò>  ç   ?     é?     ÿ?     @     @  G   @  ,   a@  }   @  b   A  +   oA  þ   A     B  O   ®B     þB  "   C  4   7C     lC  8   C     ÅC  
   ÈC  ~   ÓC  y   RD     ÌD     LE     fE     E     E  #   §E  "   ËE  .   îE     F     8F     @F     SF     \F     cF     F     F     F     ©F     ¿F     ÓF     êF     
G     G     $G     -G     KG     ]G     zG  *   G     ªG     ·G     ÉG     ÖG  "   ÙG  '   üG  "   $H     GH     `H     sH     H     ¡H     ¼H     ÔH     äH     I     I     *I  #   3I     WI  	   dI     nI     tI     I      I     ºI     ÃI     ÙI     ïI     J      J     9J     SJ     \J     jJ     sJ  5   |J  	   ²J     ¼J  (   ÈJ     ñJ     K     K  	   K      K     ¶K     ¾K     ÇK  $   àK  B   L  8   HL     L     L     L     ®L     »L     KM  0   RM     M     M  	   ¤M     ®M     ÆM     ÓM     àM     ìM     ýM     N     #N     4N     KN     fN     N      N     ¨N     ¼N     ÅN     ÝN     óN     øN  	   O  1   O     AO     SO     kO     O     O     §O     ¬O     ÊO     êO  _   ïO  W   OP  ¥   §P  (   MQ  5   vQ  ,   ¬Q  7   ÙQ  >   R  H   PR  3   R     ÍR     ëR     S  .   #S  1   RS  .   S  +   ³S      ßS  X    T  Z   YT  k   ´T  c    U  ?   U  *   ÄU     ïU     öU     V     #V     ©V     µV     ºV     ÎV     ßV  .   èV  =   W  3   UW  4   W  #   ¾W  K   âW  E   .X  F   tX     »X  n   ØX  A   GY     Y     ¡Y  S   ºY  N   Z     ]Z  ,   wZ  -   ¤Z  ¡   ÒZ  .   t[      £[     Ä[  )   á[  )   \  %   5\  T   [\     °\  /   1]  `   a]  2   Â]     õ]  ^   û]     Z^     ^^  
   c^  	   n^     x^     ^  
   ^     ©^     Ä^     ×^     à^     ò^     _     
_     !_     ?_     R_     k_      p_     _  /   ¨_     Ø_     å_     ë_     `     `  0   #`  7   T`  #   `     °`     ¼`     Ê`     é`  $   ü`     !a     0a     4a     Ra  *   oa     a  U   ªa  B    b  H   Cb  C   b  7   Ðb  @   c  ;   Ic     c  ]   c  {   øc  |   td     ñd     e  +   f  #   1f  G   Uf     f  ¢   4g     ×g  u   jh  D   àh  P   %i  /   vi     ¦i     ¼i  )   Üi     j     j  &   2j  :   Yj     j  1   ­j  6   ßj  .  k  +   El     ql  '   l  2   ¬l     ßl  %   ýl  ^   #m     m  &   ¢m     Ém     àm     ÷m     ûm  	   n     n     $n     1n     9n     @n     Fn     Ln  &   Rn     yn     n     n  F   n  	   ßn  8   én  =   "o  Â  `o     #q  +   @q  ,   lq  z   q  ?   r  Y   Tr  ]   ®r  A  s  S   Nw    ¢w  ë  ±y  {   {  ç  |  ñ   ~  >   ó~  ?   2     r               ¥     ·     È     Ü     õ          2     B  -   ]  ?        Ë     â     ü          )  	   F     P     R  >   d  2   £  1   Ö  1     a   :          ·  0  Í  6   þ     5     K     X     q          ¦     ­     »     ×     ä     ñ                    ,     8     E     U  &   h  	          '   µ     Ý     ê  0   ñ  $   "  B   G  1     6   ¼     ó  6      	   7     A     H     O     \     i      p          ¡     ª     ½     Ñ     Þ     ë     ô       %        ¶     É     â  )   ø  $   "     G     e     v  	                  ¸     ¿  3   Ì        9        Y  `   u  $   Ö     û               &     9     U     h       	        ¤     «     ·     Ó  &   á               (     5     <     C     J     `     v  	          '   ¬  )   Ô     þ            	        #     1     D     `  #   w           ¼     Â  -   Ï     ý       *   5  6   `  3     Z   Ë     &  S   -  ?     <   Á  4   þ     3     ;     T     [     h  .   u  ç   ¤               ¦     ¶  6   ¾  -   õ     #  o   ¥  )     ï   ?     /  O   C       "   ©  1   Ì  !   þ  *         K  
   N  l   Y  o   Æ     6     ¶  !   Ñ  '   ó       +   "     N  .   m  "        ¿     Æ     Ó     Ú     á                     -     I     Y  -   l          ­     ¶     ¿     Ý     í     	  *        ;     H  	   \     f     i  '        °     Í     ì          $  !   C     e               ¹     Ò     ß  *   æ            	   ,     6     U     \     v     }          «     Ê     æ       	   #     -     :     B  5   I                    ®     Í     c  	   k     u               ¦  $   Á  R   æ  ?   9     y                    ¨     3  0   :     k     r  	             ©     ¶     Ã     Ê     Û     ì                 '      =      M      `      g   	   z                  ­      ´   	   Á   3   Ë      ÿ      ¡     %¡     >¡     N¡     d¡     k¡  '   ¡     ¬¡  _   ³¡  H   ¢     \¢  -   ð¢  .   £  -   M£  7   {£  >   ³£  E   ò£  $   8¤     ]¤     |¤     ¤  .   ¶¤  *   å¤  *   ¥  #   ;¥     _¥  X   {¥  O   Ô¥  k   $¦  L   ¦  9   Ý¦  0   §     H§     O§     \§     v§     ü§     ¨     
¨     ¨     '¨     0¨  3   P¨  &   ¨  4   «¨  &   à¨  9   ©  9   A©  9   {©     µ©  \   Ñ©  6   .ª     eª     {ª  9   ª  9   Ëª     «  "   «  (   ?«  £   h«  '   ¬  !   4¬     V¬  "   i¬  0   ¬     ½¬  T   Ý¬  f   2­  /   ­  ?   É­  9   	®     C®  U   J®      ®     §®  
   ®®  	   ¹®     Ã®     Ó®     é®  $   ù®     ¯     1¯     A¯     Z¯     s¯     y¯     ¯     ¡¯     º¯     Í¯  #   Ò¯     ö¯  5   °     C°     S°     Y°     r°     °  '   °  7   ½°     õ°     ±     !±  $   .±     S±  $   l±     ±     ±  *   ¢±     Í±  /   ë±     ²  ?   .²  B   n²  3   ±²  3   å²  9   ³  3   S³  3   ³     »³  ]   Î³  {   ,´  q   ¨´     µ  w   ¨µ  *    ¶  *   K¶  ?   v¶  G   ¶¶  W   þ¶  G   V·  V   ·  P   õ·  ]   F¸  !   ¤¸     Æ¸      Ü¸  *   ý¸     (¹     D¹  '   K¹  3   s¹     §¹  -   Ã¹  (   ñ¹  .  º  $   I»     n»  $   »  *   ­»  !   Ø»     ú»  d   ¼  (   ¼     ¨¼     Ç¼     ã¼     ÿ¼     ½     ½     )½     0½     @½     G½     K½  	   O½  	   Y½  )   c½  (   ½     ¶½     º½  F   ¾½     ¾  8   ¾  <   L¾   
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            â Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        â Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   to access your account?  "%s" added to collection "%s" "%s" already in collection "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (%(username)s's collection) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s ago %(username)s's Privileges %(username)s's collections %(username)s's media %(username)s's profile (remove) + -- Select -- -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a>'s account <a href="%(user_url)s">%(username)s</a>'s collections <a href="%(user_url)s">%(username)s</a>'s media <a href="%(user_url)s">%(username)s</a>'s media with tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). A collection with that slug already exists for this user. Account settings saved Action Taken Active Reports Filed Active Reports on %(username)s Active Users Add Add Blog Post Add a Persona email address Add a collection Add a comment Add a new collection Add an OpenID Add attachment Add media Add new Row Add this comment Add to a collection Add your media Add â%(media_title)sâ to a collection Added All reports on %(username)s All reports that %(username)s has filed All rights reserved Allow Almost done! Your account still needs to be activated. An acceptable processing file was not found An email has been sent with instructions on how to change your password. An email should arrive in a few moments with instructions on how to do so. An entry with that slug already exists for this user. An error occured Applications with access to your account can:  Atom feed Attachments Authorization Authorization Complete Authorization Finished Authorize BANNED until %(expiration_date)s Bad Request Ban User Ban the user Banned Indefinitely Bio Browse collections CAUTION: CSRF cookie not present. This is most likely the result of a cookie blocker or somesuch.<br/>Make sure to permit the settings of cookies for this domain. Cancel Cannot link theme... no theme set
 Change account settings Change your information Change your password. Changing %(username)s's account settings Changing %(username)s's email Changing %(username)s's password Clear empty Rows Closed Reports Collected in Collection Collection "%s" added! Comment Comment Preview Copy and paste this into your client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Could not read the image file. Could not send password recovery email as your username is inactive or your account's email address has not been verified. Couldn't find someone with that username. Create Create a Blog Create an account! Create new collection Create one here! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Created Delete Delete Blog Delete a Persona email address Delete an OpenID Delete collection %(collection_title)s Delete my account Delete permanently Delete the content Demote Deny Description Description of Report Description of this collection Description of this work Do you want to authorize  Don't have an account yet? Don't have one yet? It's easy! Don't process eagerly, pass off to celery Download Download model Edit Edit Blog Edit Metadata Edit profile Editing %(collection_title)s Editing %(media_title)s Editing %(username)s's profile Editing attachments for %(media_title)s Email Email address Email me when others comment on my media Email verification needed Enable insite notifications about events. Enter the URL for the media to be featured Enter your old password to prove you own this account. Enter your password to prove you own this account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Explore FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File File Format File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Forgot your password? Front Go to page: Granted Here you can track the state of media being processed on this instance. Here's a spot to tell others about yourself. Hi %(username)s,

to activate your GNU MediaGoblin account, open the following URL in
your web browser:

%(verification_url)s Hi %(username)s,
%(comment_author)s commented on your post (%(comment_url)s) at %(instance_name)s
 Hi there, welcome to this MediaGoblin site! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 I am sure I want to delete this I am sure I want to remove this item from the collection ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. If you are that person but you've lost your verification email, you can <a href="%(login_url)s">log in</a> and resend it. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out In case it doesn't: Include a note Invalid User name or email address. Invalid file given for media type. Is there another way to manage featured media? Last 10 successful uploads License License preference Location Log in Log in to create an account! Log out Logging in failed! Mark all read Max file size: {0} mb Media in-processing Media processing panel Media tagged with: %(tag_name)s MediaGoblin logo MetaData Metadata Metadata for "%(media_name)s" Most recent media Must provide an oauth_token. Name Name of user these media entries belong to New comments New email address New password No No OpenID service was found for %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. No failed entries! No media in-processing No open reports found. No processed entries, yet! No request token found. No users found. Nothing is currently featured. OAuth client connections Object Height Offender Old link found for "%s"; removing.
 Old password Older â Oops! Oops, your comment was empty. OpenID OpenID was successfully removed. OpenID's Operation not allowed Or login with OpenID! Or login with Persona! Or login with a password! Or register with OpenID! Or register with Persona! Original Original file PDF file Password Path to the csv file containing metadata information. Persona's Perspective Please check your entries and try again. Post new media as you Powered by <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, a <a href="http://gnu.org/">GNU</a> project. Primary Privilege Profile changes saved Promote RESOLVED Really delete %(title)s? Really delete collection: %(title)s? Really delete user '%(user_name)s' and all related media/comments? Really remove %(media_title)s from %(collection_title)s? Reason Reason for Reporting Recover password Redirect URI Released under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Source code</a> available. Remove Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Resend verification email Resent your verification email. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Save Save changes Secondary See your information (e.g profile, media, etc...) Send instructions Send the user a message Separate tags by commas. Set password Set your new password Side Sign in to create an account! Skipping "%s"; already set up.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Someone has registered an account with this username, but it still has to be activated. Sorry Dave, I can't let you do that!</p><p>You have tried  to perform a function that you are not allowed to. Have you been trying to delete all user accounts again? Sorry, I don't support that file type :( Sorry, a user with that email address already exists. Sorry, a user with that name already exists. Sorry, an account is already registered to that OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Sorry, authentication is disabled on this instance. Sorry, comments are disabled. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Sorry, registration is disabled on this instance. Sorry, reporting is disabled on this instance. Sorry, the OpenID server could not be found Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Sorry, this audio will not work because 
	your web browser does not support HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Sorry, this video will not work because
      your web browser does not support HTML5 
      video. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Stay logged in Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Tagged with Tags Take away privilege Terms of Service Tertiary That OpenID is not registered to this account. That Persona email address is not registered to this account. The Persona email address was successfully removed. The blog was not deleted because you have no rights. The client {0} has been registered! The collection was not deleted because you didn't check that you were sure. The item was not removed because you didn't check that you were sure. The media was not deleted because you didn't check that you were sure. The name of the OAuth client The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. The request sent to the server is invalid, please double check it The slug can't be empty The title can't be empty The title part of this collection's address. You usually don't need to change this. The title part of this media's address. You usually don't need to change this. The user id is incorrect. The verification key or user id is incorrect The verification key or user id is incorrect. There doesn't seem to be a page at this address. Sorry!</p><p>If you're sure the address is correct, maybe the page you're looking for has been moved or deleted. There doesn't seem to be any media here yet... These uploads failed to process: This address contains errors This field does not take email addresses. This field is required for public clients This field requires an email address. This is where your media will appear, but you don't seem to have added anything yet. This site is running <a href="http://mediagoblin.org">MediaGoblin</a>, an extraordinarily great piece of media hosting software. This user hasn't filled in their profile (yet). This will be visible to users allowing your
                application to authenticate as them. This will be your default license on upload forms. Title To add your own media, place comments, and more, you can log in with your MediaGoblin account. Top Type UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Username Username or Email Username or email Value Verification cancelled Verification of %s failed: %s Verify your email! Video transcoding failed View View all of %(username)s's media View most recent media View on <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL WebM file (VP8/Vorbis) WebM file (Vorbis codec) Website What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Woohoo! Submitted! Woohoo! edited blogpost is submitted Wrong password Yes Yes, really delete my account You added the attachment %s! You already have a collection called "%s"! You are Banned. You are about to delete an item from another user's collection. Proceed with caution. You are about to delete another user's Blog. Proceed with caution. You are about to delete another user's collection. Proceed with caution. You are about to delete another user's media. Proceed with caution. You are editing a user's profile. Proceed with caution. You are editing another user's collection. Proceed with caution. You are editing another user's media. Proceed with caution. You are logged in as You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! You can get a modern web browser that 
      can play this video at <a href="http://getfirefox.com">
      http://getfirefox.com</a>! You can now log in using your new password. You can only edit your own profile. You can track the state of media being processed for your gallery here. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> for formatting. You can use <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> for formatting. You can't delete your only OpenID URL unless you have a password set You can't delete your only Persona email address unless you have a password set. You cannot take action against an administrator You deleted the Blog. You deleted the collection "%s" You deleted the item from the collection. You deleted the media. You have been banned You have to select or add a collection You must be logged in so we know who to send the email to! You must provide a file. You need to confirm the deletion of your account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the You've already verified your email address! Your OAuth clients Your OpenID url was saved successfully. Your Persona email address was saved successfully. Your comment has been posted! Your email address has been verified. Your email address has been verified. You may now login, edit your profile, and submit images! Your last 10 successful uploads Your password was changed successfully an unknown application commented on your post day feature management panel. feed icon hour indefinitely log out minute month newer older unoconv failing to run, check log file until %(until_when)s week year {files_uploaded} out of {files_attempted} files successfully submitted â Newer â Blog post by <a href="%(user_url)s">%(username)s</a> â Browsing media by <a href="%(user_url)s">%(username)s</a> Project-Id-Version: GNU MediaGoblin
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-07-10 12:32-0500
PO-Revision-Date: 2014-07-10 17:32+0000
Last-Translator: cwebber <cwebber@dustycloud.org>
Language-Team: Chinese (Taiwan) (http://www.transifex.com/projects/p/mediagoblin/language/zh_TW/)
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 
è©è«åå ± #%(report_id)s 
åªé«åå ± #%(report_id)s
               
å·²çµæ¡åå ± #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
â ç± <a href="%(user_url)s"
class="comment_authorlink">%(username)s</a> ç¼ä½
       
          <a href="%(user_url)s"> %(user_name)s</a>
          çå§å®¹å·²è¢«åªé¤
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        â è©²åªé«çåå ±ä¾èª <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    å¨éè£¡æ¨å¯ä»¥çå°å°æªèççä½¿ç¨èåå ±
   
å¨éè£¡æ¨å¯ä»¥æ¥è©¢ä½¿ç¨èï¼ä»¥é²è¡åé èç½®ã   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.  å­åæ¨çå¸³èåï¼ ã%sãå å¥ã%sãèè ã%sãå·²ç¶å¨ã%sãèè å¼µè²¼è©è«æ¸ %(blog_owner_name)s's Blog %(collection_title)s (%(username)s çèè) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s å %(username)s's Privileges %(username)s çèè %(username)sçåªé« %(username)s çåäººæªæ¡  (ç§»é¤) + â è«é¸æ â -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a> çå¸³è <a href="%(user_url)s">%(username)s</a> çèè <a href="%(user_url)s">%(username)s</a> çåªé« æ¨ç±¤çº <a href="%(tag_url)s">%(tag)s</a> ç <a href="%(user_url)s">%(username)s</a> çåªé« <em> Go to list view </em> <h2>å¯éåå ±</h2> <strong>ç§å¯</strong> â OAuth ç¨æ¶ç¨å¼å¯ä»¥å° GNU MediaGoblin ç«å°ç¼éä¸è¢«ä½¿ç¨èä»£çææªçè«æ± (ä¾å¦ä¼ºæç«¯çç¨æ¶ç¨å¼)ã
<strong>å¬é</strong> â OAuth ç¨æ¶ç¨å¼ç¡æ³å° GNU MediaGoblin ç«å°ç¼éç§å¯çè«æ± (ä¾å¦å®¢æ¶ç«¯ç JavaScript ç¨æ¶ç¨å¼)ã éåä½¿ç¨èå·²ç¶æä½¿ç¨è©²ç°¡ç¨±çèèäºã å¸³èè¨­å®å·²å²å­ ç¼çæé å·²éåºçç¾è¡åå ± Active Reports on %(username)s æ´»åä¸­çä½¿ç¨è å¢å  Add Blog Post æ°å¢ Persona email ä½å æ°å¢èè æ°å¢è©è« æ°å¢æ°çèè æ°å¢ OpenID æ°å¢éä»¶ æ°å¢åªé« Add new Row å¢å è©è« å å¥è³èè å å¥æ¨çåªé« å å¥ â%(media_title)sâ è³èè æ°å¢æ¼ All reports on %(username)s All reports that %(username)s has filed çæ¬ææ åè¨± å¿«å®æäºï¼ä½æ¨éè¦åç¨æ¨çå¸³èã æ¾ä¸å°å¯æ¶åçå¾èçæªæ¡ ä¿®æ¹å¯ç¢¼çæç¤ºå·²ç¶ç±é»å­éµä»¶å¯éå°æ¨çä¿¡ç®±ã åç¨æ­¥é©ç email å°æå¯å°æ¨çä¿¡ç®±ã éåä½¿ç¨èå·²ç¶æä½¿ç¨è©²ç°¡ç¨±çé ç®äºã ç¼çé¯èª¤ è©²ç¨å¼å°å¯ééæ¨çå¸³èé²è¡ä»¥ä¸åä½ï¼ Atom feed éä»¶ èªè¨¼ èªè¨¼å®æ èªè¨¼å®æ èªè¨¼ BANNED until %(expiration_date)s é¯èª¤çè«æ± Ban User å°éè©²ä½¿ç¨è Banned Indefinitely èªæä»ç´¹ çè¦½èè CAUTION: è·¨ç¶²ç«å­å (CSRF) ç cookie ä¸å­å¨ï¼æå¯è½æ¯ cookie é»æç¨å¼ä¹é¡çç¨å¼å°è´çã<br/>è«åè¨±æ­¤ç¶²åç cookie è¨­å®ã åæ¶ ç¡æ³é£çµä½æ¯â¦æ²ææ­¤ä½æ¯
 æ´æ¹å¸³èè¨­å® è®æ´æ¨çåäººè³æ æ´æ¹æ¨çå¯ç¢¼ã æ­£å¨æ¹è® %(username)s çå¸³èè¨­å® æ´æ¹ %(username)s ç email ä½å æ´æ¹ %(username)s çå¯ç¢¼ Clear empty Rows å·²çµæ¡çåå ± èéäº èè èèã%sãæ°å¢å®æï¼ è©è« è©è«é è¦½ è«å°ä»¥ä¸æå­è¤è£½ä¸¦è²¼å°æç¨ç¨å¼ä¸ï¼ è¤è£½å°å¬æå­å²å¤±æã ç¡æ³é£çµã%sãï¼%s å­å¨ï¼ä¸ä¸æ¯ç¬¦èé£çµ
 ç¡æ³è®ååçæªæ¡ã ç¡æ³å³éå¯ç¢¼åå¾©ä¿¡ä»¶ï¼å çºæ¨çä½¿ç¨èåç¨±å·²å¤±æææ¯å¸³èå°æªèªè­ã æ¾ä¸å°ç¸éçä½¿ç¨èåç¨±ã å»ºç« Create a Blog å»ºç«ä¸åå¸³èï¼ æ°å¢æ°çèè å¨éè£¡å»ºç«ä¸åå§ï¼ Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. å»ºç«æ¼ åªé¤ Delete Blog åªé¤ Persona email ä½å åªé¤ OpenID Delete collection %(collection_title)s åªé¤æçå¸³è æ°¸ä¹åªé¤ åªé¤å§å®¹ Demote æçµ æè¿° Description of Report éåèèçæè¿° éåä½åçæè¿° æ¨åæ éæ²æå¸³èåï¼ æ²æå¸³èåï¼éå¸³èå¾ç°¡å®ï¼ Don't process eagerly, pass off to celery ä¸è¼ ä¸è¼æ¨¡å ç·¨è¼¯ Edit Blog Edit Metadata ç·¨è¼¯åäººæªæ¡ ç·¨è¼¯ %(collection_title)s ç·¨è¼¯ %(media_title)s ç·¨è¼¯ %(username)s çåäººæªæ¡ ç·¨è¼¯ %(media_title)s çéä»¶ Email Email ä½å ç¶æäººå°æçåªé«è©è«æå¯ä¿¡çµ¦æ éè¦èªè­é»å­éµä»¶ åç¨æ´»åçç«å§éç¥ã Enter the URL for the media to be featured è¼¸å¥æ¨çèå¯ç¢¼ä¾è­ææ¨ææéåå¸³èã è¼¸å¥æ¨çå¯ç¢¼ä¾è­ææ¨ææéåå¸³èã Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. æ¢ç´¢ FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel æªæ¡ æªæ¡æ ¼å¼ éåºåå ± File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> å¿äºå¯ç¢¼åï¼ æ­£é¢ è·³å°é æ¸ï¼ Granted æ­¤èæ¨å¯ä»¥è¿½è¹¤æ¬ç«å°èçåªé«ççæã éåå°æ¹è½è®æ¨åä»äººä»ç´¹èªå·±ã %(username)s æ¨å¥½ï¼

è¦åå GNU MediaGoblin å¸³èï¼è«å¨æ¨ççè¦½å¨ä¸­æéä¸é¢çç¶²åï¼

%(verification_url)s %(username)s æ¨å¥½ï¼
%(comment_author)s å¨ %(instance_name)s å°æ¨çå§å®¹ (%(comment_url)s) å¼µè²¼è©è«
 å¿ï¼æ­¡è¿ä¾å° MediaGoblin ç«å°ï¼  æ¨å¥½ï¼

æåè¦ç¢ºèªæ¨æ¯ %(username)s ãå¦æç¢ºå¯¦å¦æ­¤ï¼è«é»é¸ä»¥ä¸é£çµä¾èªè¨¼æ¨çæ° email ä½åã

%(verification_url)s

å¦ææ¨ä¸æ¯ %(username)s ææ²æè¦æ±ä¿®æ¹ email ä½åï¼è«å¿½ç¥éå°éµä»¶ã How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? ä½æ¯èçç®éé£çµå·²ç¶æ¾å°ä¸¦ç§»é¤ã
 æç¢ºå®æè¦åªé¤éååªé« æç¢ºå®æè¦å¾èèä¸­ç§»é¤æ­¤é ç® ID Identifier å¦æé£ email ä½ç½® (è«æ³¨æå¤§å°å¯«) å·²ç¶è¨»åï¼å¯«æä¿®æ¹å¯ç¢¼æ­¥é©ç email å·²ç¶éåºã å¦ææ¨å°±æ¯æ¬äººä½æ¯æäºèªè­ä¿¡ï¼æ¨å¯ä»¥ <a href="%(login_url)s">ç»å¥</a> ç¶å¾ééä¸æ¬¡ã If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says  %(media_title)s çç§ç å£åå¾å¤§çå¥å¸æç¤ºæå å¦æä»ç¶ç¡æ³èªè­ï¼æ¨å¯ä»¥ï¼ å è¨» ç¡æçä½¿ç¨èåç¨±æ email ä½ç½®ã æå®é¯èª¤çåªé«é¡å¥ï¼ Is there another way to manage featured media? æè¿ 10 æ¬¡æåä¸å³çç´é ææ¬ ææ¬åå¥½ ä½ç½® ç»å¥ ç»å¥ä»¥åµå»ºä¸åå¸³èï¼ ç»åº ç»å¥å¤±æï¼ å¨é¨æ¨ç¤ºçºå·²è® æå¤§æªæ¡å¤§å°ï¼{0} mb åªé«èçä¸­ åªé«èçé¢æ¿ éååªé«å·æä»¥ä¸æ¨ç±¤ï¼%(tag_name)s MediaGoblin æ¨èª MetaData Metadata Metadata for "%(media_name)s" ææ°çåªé« å¿é æä¾ oauth_tokenã åç¨± Name of user these media entries belong to æ°çè©è« æ°ç email ä½å æ°å¯ç¢¼ No æ¾ä¸å° %s ç OpenID æå No active reports filed on %(username)s æ­¤ä½æ¯æ²æç´ æç®é
 æ¾ä¸å°å·²çµæ¡çåå ±ã æ²æå¤±æçç´éï¼ æ²ææ­£å¨èçä¸­çåªé« æ²æå°æªèççåå ±ã ç¾å¨éæ²æèççç´éï¼ æ¾ä¸å°è«æ±ç tokenã æ¾ä¸å°è©²ä½¿ç¨èã Nothing is currently featured. OAuth ç¨æ¶ç¨å¼é£ç· ç©ä»¶é«åº¦ è¢«å æ¾å°ã%sãèçé£çµï¼åªé¤ä¸­ã
 èçå¯ç¢¼ æ´èç â ç³ç³ï¼ åï¼æ¨çè©è«æ¯ç©ºçã OpenID OpenID å·²æåç§»é¤ã OpenID æä½ä¸åè¨± ææ¯ä½¿ç¨ OpenID ç»å¥ï¼ ææ¯ä½¿ç¨ Persona ç»å¥ï¼ ææ¯ä½¿ç¨å¯ç¢¼ç»å¥ï¼ ææ¯ä½¿ç¨ OpenID è¨»åï¼ ææ¯ä½¿ç¨ Persona è¨»åï¼ åå§æª åå§æªæ¡ PDF æª å¯ç¢¼ Path to the csv file containing metadata information. Persona ç éè¦ è«æª¢æ¥é ç®ä¸¦éè©¦ã ä»¥æ¨çåç¾©å¼µè²¼æ°åªé« æ¬ç«ä½¿ç¨ <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>ï¼éæ¯ä¸å <a href="http://gnu.org/">GNU</a> å°æ¡ã Primary Privilege åäººæªæ¡ä¿®æ¹å·²å²å­ Promote å·²ç¶èç ççè¦åªé¤ %(title)s? Really delete collection: %(title)s? ççè¦åªé¤ä½¿ç¨èã%(user_name)sãä»¥åææç¸éçåªé«èè©è«ï¼ ç¢ºå®è¦å¾ %(collection_title)s ç§»é¤ %(media_title)s åï¼ çç± åå ±çç± æ¾åå¯ç¢¼ éå®å URI ä»¥ <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a> ææ¬éåºãåæ<a href="%(source_link)s">åå§ç¢¼</a>ã ç§»é¤ Remove %(media_title)s from %(collection_title)s åå ± Report #%(report_number)s Report ID åå ±ç®¡çé¢æ¿ åå ±åªé« åå ±é¢æ¿ åå Reported Comment Reported Content Reported Media Entry å·²ç¶åå ±çè©è« åå ±è©²è©è« åå ±è©²åªé«é ç® ééèªè­ä¿¡ ééèªè­ä¿¡ã èç èçéé åå ± å·²èç åå°åå ±é¢æ¿ Return to Users Panel å²å­ å²å­è®æ´ Secondary æª¢è¦æ¨çè³è¨ (å¦åäººè³æãåªé«â¦ç­) éåºæç¤º å¯è¨æ¯å°è©²ä½¿ç¨è ç¨éèåéæ¨ç±¤ã è¨­å®æ°å¯ç¢¼ è¨­å®æ¨çæ°å¯ç¢¼ å´é¢ ç»å¥ä»¥å»ºç«å¸³èï¼ è·³éã%sãï¼å·²ç¶å»ºç½®å®æã
 ç°¡ç¨± Someone has registered an account with this username, but it still has
        to be activated. æäººç¨äºéåå¸³èç»éäºï¼ä½æ¯éåå¸³èéè¦è¢«åç¨ã Dave å°ä¸èµ·ï¼æä¸è½è®ä½ éæ¨£åï¼</p><p>æ¨æ­£å¨è©¦èæä½ä¸åè¨±æ¨ä½¿ç¨çåè½ãæ¨æç®åªé¤ææä½¿ç¨èçå¸³èåï¼ æ±æ­ï¼æä¸æ¯æ´éæ¨£çæªæ¡æ ¼å¼ :( æ±æ­ï¼æ­¤ email ä½ç½®å·²ç¶è¢«è¨»åäºã æ±æ­ï¼éåä½¿ç¨èåç¨±å·²ç¶å­å¨ã æ±æ­ï¼æå¸³èå·²ç¶ç»è¨äºæ¨è¼¸å¥ç OpenIDã æ±æ­ï¼æå¸³èå·²ç¶ç»è¨äºæ¨è¼¸å¥ç Persona emailã æ±æ­ï¼æå¸³èå·²ç¶ç»è¨äºæ¨è¼¸å¥ç Persona email ä½åã æ±æ­ï¼æ¬ç«å·²ç¶ééèªè¨¼ã æ±æ­ï¼è©è«è¢«ééäºã æ±æ­ï¼æ¾ä¸å°è©²åå ±ã Sorry, no such user found. Sorry, no user by username '{username}' exists æ±æ­ï¼æ¬ç«å·²ç¶ééè¨»ååè½ã æ±æ­ï¼æ¬ç«å·²ç¶ééåå ±åè½ã æ±æ­ï¼æ¾ä¸å° OpenID ä¼ºæå¨ æ±æ­ï¼æªæ¡å¤ªå¤§äºã Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. æ±æ­ï¼æ­¤è²é³ç¡æ³æ­æ¾ï¼å çºæ¨ççè¦½å¨ä¸æ¯æ´ HTML5 é³è¨ã Sorry, this video will not work because
          your web browser does not support HTML5 
          video. æ±æ­ï¼ç±æ¼æ¨ççè¦½å¨ä¸æ¯æ´ HTML5 å½±çï¼æ¬å½±çç¡æ³æ­æ¾ æ±æ­ï¼ä¸å³è©²æªæ¡å°æè¶éæ¨çä¸å³éå¶ã æ±æ­ï¼æ¨å·²ç¶ç¢°å°äºæ¨çä¸å³éå¶ã çæ ä¿æç»å¥ å·²è¨é± %s çè©è«ï¼ Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. æ¨ç±¤ æ¨ç±¤ ç§»é¤æ¬é ä½¿ç¨èæ¢æ¬¾ Tertiary OpenID å°æªç»è¨å°æ­¤å¸³è è©² Persona email ä½åå°æªç»è¨å°æ­¤å¸³èã Persona email ä½åå·²æåç§»é¤ã The blog was not deleted because you have no rights. OAuth ç¨æ¶ç¨å¼ {0} è¨»åå®æï¼ ç±æ¼æ¨æ²æå¾é¸ç¢ºèªï¼è©²èèæ²æè¢«ç§»é¤ã ç±æ¼æ¨æ²æå¾é¸ç¢ºèªï¼è©²é ç®æ²æè¢«ç§»é¤ã ç±æ¼æ¨æ²æå¾é¸ç¢ºèªï¼è©²åªé«æ²æè¢«ç§»é¤ã OAuth ç¨æ¶ç¨å¼çåç¨± æ­¤æç¨ç¨å¼çéå®å URIï¼æ¬æ¬ä½å¨å¬éé¡åç OAuth ç¨æ¶ç¨å¼çºå¿å¡«ã å³éå°æ­¤ä¼ºæå¨çè«æ±ç¡æï¼è«åä¸ç¢ºèª ç°¡ç¨±ä¸è½çºç©ºç½ æ¨é¡ä¸è½æ¯ç©ºç æ­¤èèç¶²åçæ¨é¡é¨ä»½ï¼éå¸¸ä¸éè¦ä¿®æ¹ã æ­¤åªé«ç¶²åçæ¨é¡é¨ä»½ãéå¸¸ä¸éè¦ä¿®æ¹ã ä½¿ç¨è ID ä¸æ­£ç¢º èªè­ç¢¼ææ¯ä½¿ç¨è ID é¯èª¤ èªè¨¼éé°æä½¿ç¨è ID ä¸æ­£ç¢ºã ä¸å¥½ææï¼çèµ·ä¾éåç¶²åä¸æ²æç¶²é ã</p><p>å¦ææ¨ç¢ºå®éåç¶²åæ¯æ­£ç¢ºçï¼æ¨å¨å°æ¾çé é¢å¯è½å·²ç¶ç§»åææ¯è¢«åªé¤äºã é£è£¡å¥½åéæ²æä»»ä½çåªé«â¦ ç¡æ³èçéäºä¸å³å§å®¹ï¼ æ¬ç¶²ååºé¯äº æ¬æ¬ä½ä¸æ¥å email ä½ç½®ã æ¬æ¬ä½å¨å¬éé¡åçç¨æ¶ç¨å¼çºå¿å¡« æ¬æ¬ä½éè¦ email ä½ç½®ã æ­¤èæ¯æ¨çåªé«æåºç¾çå°æ¹ï¼ä½æ¯ä¼¼ä¹éæ²æå å¥ä»»ä½æ±è¥¿ã æ¬ç«ä½¿ç¨ <a href="http://mediagoblin.org">MediaGoblin</a> â è¶è®çåªé«åäº«æ¶ç«è»é«ã éåä½¿ç¨è(é)æ²æå¡«å¯«åäººæªæ¡ã æ¬æè¿°å°æè¢«é²è¡æç¨ç¨å¼èªè¨¼çä½¿ç¨èçå°ã å¨ä¸å³é é¢ï¼éå°ææ¯æ¨é è¨­çææ¬æ¨¡å¼ã æ¨é¡ æ¨å¯ä»¥ç»å¥æ¨ç MediaGoblin å¸³èä»¥é²è¡ä¸å³åªé«ãå¼µè²¼è©è«ç­ç­ã é é¢ é¡å UnBan User Unfeature Update Metadata ä½¿ç¨èç®¡çé¢æ¿ ä½¿ç¨èé¢æ¿ è©²ä½¿ç¨èå°è¢«å°éï¼ç´å°ï¼ User: %(username)s ä½¿ç¨èåç¨± ä½¿ç¨èåç¨±æ email ä½¿ç¨èåç¨±æ email Value èªè¨¼å·²åæ¶ %s çèªè¨¼å¤±æï¼%s ç¢ºèªæ¨çé»å­éµä»¶ å½±åè½ç¢¼å¤±æ View æ¥ç %(username)s çå¨é¨åªé« View most recent media å¨ <a href="%(osm_url)s">OpenStreetMap</a> ä¸è§ç è­¦åï¼ä¾èª WebGL WebM æªæ¡ (VP8/Vorbis) WebM æªæ¡ (Vorbis ç·¨ç¢¼) ç¶²ç« è«åæ¨è¦å¦ä½èçéé åå ±ï¼ What is a Primary Feature? What is a Secondary Feature? æ¨è¦åèµ°ä»åªäºæ¬éï¼ å å¥æé åå ±æé çºä»éº¼æ¨è¦å°éè©²ä½¿ç¨èï¼ ååï¼PO ä¸å»å¦ï¼ Woohoo! edited blogpost is submitted å¯ç¢¼é¯èª¤ Yes æ¯çï¼æççè¦ææçå¸³èåªé¤ æ¨å ä¸äºéä»¶ã%sãï¼ æ¨å·²ç¶æä¸åç¨±åã%sãçèèäºï¼ æ¨è¢«å°éäºã æ¨æ­£å¨å¾å¥äººçèèä¸­åªé¤é ç®ï¼è«å°å¿æä½ã You are about to delete another user's Blog. Proceed with caution. æ¨æ­£å¨åªé¤å¥äººçèèï¼è«å°å¿æä½ã æ¨æ­£å¨åªé¤å¥äººçåªé«ï¼è«å°å¿æä½ã æ¨æ­£å¨ä¿®æ¹å¥äººçåäººæªæ¡ï¼è«å°å¿æä½ã æ¨æ­£å¨ä¿®æ¹å¥äººçèèï¼è«å°å¿æä½ã æ¨æ­£å¨ä¿®æ¹å¥äººçåªé«ï¼è«å°å¿æä½ã æ¨å·²ç¶ç»å¥çº æ¨ç¾å¨ä¸æ¯æ´»åä¸­çä½¿ç¨èï¼è«è¯çµ¡ç³»çµ±ç®¡çå¡ä»¥éæ°åç¨æ¨çå¸³èã You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! æ¨å¯ä»¥å¨ <a href="http://getfirefox.com">http://getfirefox.com</a> åå¾å¯ä»¥æ­æ¾æ­¤è²é³ççè¦½å¨ï¼ You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! æ¨å¯ä»¥å¨ <a href="http://getfirefox.com">http://getfirefox.com</a> åå¾å¯ä»¥æ­æ¾æ­¤å½±ççåé²çè¦½å¨ã æ¨ç¾å¨å¯ä»¥ç¨æ°çå¯ç¢¼ç»å¥äºï¼ æ¨åªè½ä¿®æ¹æ¨èªå·±çåäººæªæ¡ã æ¨å¯ä»¥å¨éè£¡è¿½è¹¤æ¨çèå»ä¸­åªé«èçççæã æ¨å¯ä»¥ä½¿ç¨
<a href="http://markdown.tw">
Markdown</a> ä¾æçã æ¨å¯ä»¥ä½¿ç¨
<a href="http://markdown.tw" target="_blank">
Markdown</a> ä¾æçã æ¨å¯ä»¥ä½¿ç¨
<a href="http://markdown.tw">
Markdown</a> ä¾æçã æ¨å¯ä»¥ä½¿ç¨ <a href="http://markdown.tw" target="_blank">Markdown</a> ä¾æçã é¤éæ¨è¨­å®äºå¸³èçå¯ç¢¼ï¼æ¨ç¡æ³åªé¤å¸³èä¸å¯ä¸ç OpenID URL é¤éæ¨è¨­å®äºå¸³èçå¯ç¢¼ï¼æ¨ç¡æ³åªé¤å¸³èä¸å¯ä¸ç Persona email ä½åã æ¨ä¸è½å°ç®¡çèé²è¡æä½ You deleted the Blog. æ¨å·²ç¶åªé¤ã%sãèèã æ¨å·²ç¶å¾è©²èèä¸­åªé¤è©²é ç®ã æ¨å·²ç¶åªé¤æ­¤åªé«ã æ¨è¢« æ¨éè¦é¸æææ¯æ°å¢ä¸åèè æ¨å¿é ç»å¥ï¼æåæç¥éä¿¡è¦éçµ¦èª°ï¼ æ¨å¿é æä¾ä¸åæªæ¡ æ¨å¿é è¦ç¢ºèªæ¯å¦åªé¤æ¨çå¸³èã æ¨å°ä¸ææ¶å° %s çè©è«éç¥ã You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the æ¨çé»å­éµä»¶å·²ç¶ç¢ºèªäºï¼ æ¨ç OAuth ç¨æ¶ç¨å¼ æ¨ç OpenID url å·²æåå²å­ã æ¨ç Persona email ä½åå·²æåå²å­ æ¨çè©è«å·²ç¶å¼µè²¼å®æï¼ æ¨ç email ä½åå·²èªè¨¼ã æ¨ç email ä½åå·²è¢«èªè­ãæ¨å·²ç¶å¯ä»¥ç»å¥ï¼ç·¨è¼¯æ¨çåäººæªæ¡ä¸¦ä¸å³åçï¼ æ¨çæè¿ 10 æ¬¡æåä¸å³çç´é æ¨çå¯ç¢¼å·²ç¶æåä¿®æ¹ ä¸åæªç¥çæç¨ç¨å¼ å¨æ¨çå§å®¹å¼µè²¼è©è« æ¥ feature management panel. feed åç¤º å°æ æ°¸ä¹å°éäº ç»åº å æ æ´æ°ç æ´èç unoconv ç¡æ³å·è¡ï¼è«æª¢æ¥ç´éæª å°éäºï¼æå¨ %(until_when)s è§£é¤ é± å¹´ {files_uploaded} out of {files_attempted} files successfully submitted â æ´æ°ç â Blog post by <a href="%(user_url)s">%(username)s</a> â çè¦½ <a href="%(user_url)s">%(username)s</a> çåªé« 