Þ    á     $              ,  =   -  ;   k  8   §  z   à  ?   [       o      A     L   Ï$    %  ë  +'  {   )  ç  )  ñ   {+  H   m,  M   ¶,     -     -     %-     7-     I-     Z-     n-     -     ¦-     Æ-     Û-  0   ö-  ?   '.     g.     ~.     .     ³.     È.     ß.     è.     ê.  >   ÷.  2   6/  5   i/  /   /  Z   Ï/     *0     E0    \0  9   ä1     2     52     B2     W2     v2     2     2     2     ±2     Â2     Ð2     å2     ó2  	   3     3     3     )3     =3  )   L3     v3     |3  '   3     À3     Ô3  6   Ú3  +   4  H   =4  J   4  5   Ñ4     5  .   5  	   G5     Q5     ]5     k5     5  	   5      £5     Ä5     Ð5     Ù5     æ5     ú5     þ5     6     6     ´6  "   »6     Þ6     ö6     7  (   $7     M7      k7     7     7     ¬7  
   ¹7     Ä7     Û7     ã7  %   ó7  !   8  4   ;8     p8  z   8  )   
9     49     ;9     I9     \9     r9     9     9     ¯9     È9     Ð9     ×9     ã9     :  &   :     ::     L:     _:     r:     y:     ~:     :      :     ¿:     Ø:     ò:     ;  )   ,;     V;     _;     n;  	   s;     };     ;     ;     µ;     Í;  '   ì;     <     <  (   (<     Q<  )   k<  *   <  6   À<  2   ÷<  Z   *=     =  S   =  ?   á=  <   !>  4   ^>     >     >     ´>     ¹>     Å>  .   Ò>  ç   ?     é?     ÿ?     @     @  G   @  ,   a@  }   @  b   A  +   oA  þ   A     B  O   ®B     þB  "   C  4   7C     lC  8   C     ÅC  
   ÈC  ~   ÓC  y   RD     ÌD     LE     fE     E     E  #   §E  "   ËE  .   îE     F     8F     @F     SF     \F     cF     F     F     F     ©F     ¿F     ÓF     êF     
G     G     $G     -G     KG     ]G     zG  *   G     ªG     ·G     ÉG     ÖG  "   ÙG  '   üG  "   $H     GH     `H     sH     H     ¡H     ¼H     ÔH     äH     I     I     *I  #   3I     WI  	   dI     nI     tI     I      I     ºI     ÃI     ÙI     ïI     J      J     9J     SJ     \J     jJ     sJ  5   |J  	   ²J     ¼J  (   ÈJ     ñJ     K     K  	   K      K     ¶K     ¾K     ÇK  $   àK  B   L  8   HL     L     L     L     ®L     »L     KM  0   RM     M     M  	   ¤M     ®M     ÆM     ÓM     àM     ìM     ýM     N     #N     4N     KN     fN     N      N     ¨N     ¼N     ÅN     ÝN     óN     øN  	   O  1   O     AO     SO     kO     O     O     §O     ¬O     ÊO     êO  _   ïO  W   OP  ¥   §P  (   MQ  5   vQ  ,   ¬Q  7   ÙQ  >   R  H   PR  3   R     ÍR     ëR     S  .   #S  1   RS  .   S  +   ³S      ßS  X    T  Z   YT  k   ´T  c    U  ?   U  *   ÄU     ïU     öU     V     #V     ©V     µV     ºV     ÎV     ßV  .   èV  =   W  3   UW  4   W  #   ¾W  K   âW  E   .X  F   tX     »X  n   ØX  A   GY     Y     ¡Y  S   ºY  N   Z     ]Z  ,   wZ  -   ¤Z  ¡   ÒZ  .   t[      £[     Ä[  )   á[  )   \  %   5\  T   [\     °\  /   1]  `   a]  2   Â]     õ]  ^   û]     Z^     ^^  
   c^  	   n^     x^     ^  
   ^     ©^     Ä^     ×^     à^     ò^     _     
_     !_     ?_     R_     k_      p_     _  /   ¨_     Ø_     å_     ë_     `     `  0   #`  7   T`  #   `     °`     ¼`     Ê`     é`  $   ü`     !a     0a     4a     Ra  *   oa     a  U   ªa  B    b  H   Cb  C   b  7   Ðb  @   c  ;   Ic     c  ]   c  {   øc  |   td     ñd     e  +   f  #   1f  G   Uf     f  ¢   4g     ×g  u   jh  D   àh  P   %i  /   vi     ¦i     ¼i  )   Üi     j     j  &   2j  :   Yj     j  1   ­j  6   ßj  .  k  +   El     ql  '   l  2   ¬l     ßl  %   ýl  ^   #m     m  &   ¢m     Ém     àm     ÷m     ûm  	   n     n     $n     1n     9n     @n     Fn     Ln  &   Rn     yn     n     n  F   n  	   ßn  8   én  =   "o  ¼  `o  L   q  J   jq  F   µq  z   üq  ?   wr     ·r  l   Es  A  ²s  U   ôw    Jx  ë  Yz  {   E|  ç  Á|  ñ   ©~  t     s                  ¥     ·     É     Ú  $   î  (     $   <  $   a       1   ¡  C   Ó          3      M     n          ª     ³     µ  >   Â  :     ;   <  7   x  k   °          7  À  V  I     "   a       (        Å     ä           	  +        C     U     i               ¨     ¼     È     æ     ý  1        L     [  '   w           À  T   Ç  *     j   G  |   ²  M   /     }  J        Þ     ì  
   û               8      ?     `     p     y          ¨     ¯     É  Ã   Ò  
     E   ¡     ç           $  8   E  '   ~  4   ¦     Û     ì               %  
   A     L  E   b  1   ¨  d   Ú  ;   ?  º   {  E   6     |               ¨     ¿     Ù     ì               '     .  )   :     d  &   r          ¹     Ï     æ     í  
   ö               0     P  "   p  5     )   É     ó     ü       	        !     /     C     c  +   |  2   ¨  
   Û     æ  N   ü     K  )   j  *     c   ¿  b   #  Z     
   á  S   ì  ?   @  <     4   ½     ò     ú               0  .   B  ç   q     Y  
   u            `      7     Ç   9  s     9   u  I  ¯     ù  O        ]  "   s  A     0   Ø  L   	     V  
   _  ©   j  «        À  #   @  >   d     £     »  -   Í  8   û  .   4   -   c   
            
   ´      ¿   (   Î      ÷      ¡     !¡  '   :¡     b¡     ¡  (   ¡     Å¡     Ú¡     ã¡     ì¡  (   
¢     3¢     N¢  *   S¢     ~¢     ¢     µ¢     É¢  +   Ì¢  '   ø¢  0    £  ,   Q£  !   ~£  %    £  ,   Æ£  1   ó£      %¤     F¤     f¤     ¤     ¡¤  
   ¹¤  ;   Ä¤      ¥     ¥  	   (¥  )   2¥     \¥     c¥     ¥     ¥     §¥  "   Ä¥  &   ç¥  "   ¦  $   1¦     V¦     c¦     w¦  
   ¦  5   ¦  	   Å¦     Ï¦  6   á¦     §     .§     Ç§  	   Ï§  "   Ù§     ü§     ¨  #   ¨  $   1¨  k   V¨  C   Â¨     ©     ©     #©     9©      F©     ç©  0   î©     ª     (ª  	   Bª      Lª     mª     ª     ª     ¯ª     Àª     Ñª     æª     þª  $   «     C«  2   `«     «     «     º«  #   Ã«     ç«     ý«     ¬  	   ¬  J   (¬     s¬  #   ¬  ,   «¬     Ø¬  %   ì¬     ­  (   ­  '   @­     h­  _   u­  b   Õ­  è   8®  ?   !¯  I   a¯  8   «¯  E   ä¯  X   *°  f   °  @   ê°  .   +±  .   Z±     ±  .   ¤±  @   Ó±  D   ²  =   Y²  2   ²  X   Ê²     #³  k   ¤³     ´  f   ´  4   µ  
   7µ     Bµ  )   Xµ     µ     ¶  
   ¶     %¶     ?¶     S¶  3   \¶  J   ¶  ;   Û¶  4   ·     L·  J   e·  J   °·  L   û·     H¸  n   c¸  _   Ò¸  2   2¹  0   e¹  j   ¹  j   º  *   lº  D   º  E   Üº  Î   "»  =   ñ»  ,   /¼  '   \¼  2   ¼  B   ·¼  -   ú¼  r   (½     ½  6   &¾  h   ]¾  _   Æ¾  
   &¿     1¿     ½¿  
   Ä¿  
   Ï¿  	   Ú¿     ä¿     ô¿     À     #À     =À     PÀ     `À     À      À     ¦À     ºÀ      ÚÀ     ûÀ     Á  ,   Á     LÁ  7   cÁ     Á     ­Á     ³Á     ÓÁ     óÁ  H   Â  7   JÂ  .   Â     ±Â     ÃÂ  .   ÓÂ     Ã  $   Ã     ;Ã     QÃ  +   UÃ      Ã  1   ¢Ã     ÔÃ  [   åÃ  B   AÄ  M   Ä  M   ÒÄ  D    Å  I   eÅ  I   ¯Å     ùÅ     Æ  {   Æ  ´   Ç     ÈÇ  ¼   VÈ  L   É  :   `É     É  º   Ê  Æ   ØÊ  ¯   Ë     OÌ  x   éÌ     bÍ  G   òÍ     :Î     PÎ  ,   nÎ     Î  
   ¸Î  .   ÃÎ  R   òÎ     EÏ  6   aÏ  @   Ï  .  ÙÏ  6   Ñ     ?Ñ  *   YÑ  ;   Ñ     ÀÑ  *   ÛÑ     Ò  )   Ò  $   ºÒ  "   ßÒ     Ó      Ó     'Ó     AÓ     UÓ     \Ó     iÓ     xÓ     Ó     Ó     Ó  7   ¨Ó     àÓ     ôÓ     ýÓ  F   Ô     KÔ  8   _Ô  F   Ô   
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            â Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        â Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   to access your account?  "%s" added to collection "%s" "%s" already in collection "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (%(username)s's collection) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s ago %(username)s's Privileges %(username)s's collections %(username)s's media %(username)s's profile (remove) + -- Select -- -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a>'s account <a href="%(user_url)s">%(username)s</a>'s collections <a href="%(user_url)s">%(username)s</a>'s media <a href="%(user_url)s">%(username)s</a>'s media with tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). A collection with that slug already exists for this user. Account settings saved Action Taken Active Reports Filed Active Reports on %(username)s Active Users Add Add Blog Post Add a Persona email address Add a collection Add a comment Add a new collection Add an OpenID Add attachment Add media Add new Row Add this comment Add to a collection Add your media Add â%(media_title)sâ to a collection Added All reports on %(username)s All reports that %(username)s has filed All rights reserved Allow Almost done! Your account still needs to be activated. An acceptable processing file was not found An email has been sent with instructions on how to change your password. An email should arrive in a few moments with instructions on how to do so. An entry with that slug already exists for this user. An error occured Applications with access to your account can:  Atom feed Attachments Authorization Authorization Complete Authorization Finished Authorize BANNED until %(expiration_date)s Bad Request Ban User Ban the user Banned Indefinitely Bio Browse collections CAUTION: CSRF cookie not present. This is most likely the result of a cookie blocker or somesuch.<br/>Make sure to permit the settings of cookies for this domain. Cancel Cannot link theme... no theme set
 Change account settings Change your information Change your password. Changing %(username)s's account settings Changing %(username)s's email Changing %(username)s's password Clear empty Rows Closed Reports Collected in Collection Collection "%s" added! Comment Comment Preview Copy and paste this into your client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Could not read the image file. Could not send password recovery email as your username is inactive or your account's email address has not been verified. Couldn't find someone with that username. Create Create a Blog Create an account! Create new collection Create one here! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Created Delete Delete Blog Delete a Persona email address Delete an OpenID Delete collection %(collection_title)s Delete my account Delete permanently Delete the content Demote Deny Description Description of Report Description of this collection Description of this work Do you want to authorize  Don't have an account yet? Don't have one yet? It's easy! Don't process eagerly, pass off to celery Download Download model Edit Edit Blog Edit Metadata Edit profile Editing %(collection_title)s Editing %(media_title)s Editing %(username)s's profile Editing attachments for %(media_title)s Email Email address Email me when others comment on my media Email verification needed Enable insite notifications about events. Enter the URL for the media to be featured Enter your old password to prove you own this account. Enter your password to prove you own this account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Explore FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File File Format File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Forgot your password? Front Go to page: Granted Here you can track the state of media being processed on this instance. Here's a spot to tell others about yourself. Hi %(username)s,

to activate your GNU MediaGoblin account, open the following URL in
your web browser:

%(verification_url)s Hi %(username)s,
%(comment_author)s commented on your post (%(comment_url)s) at %(instance_name)s
 Hi there, welcome to this MediaGoblin site! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 I am sure I want to delete this I am sure I want to remove this item from the collection ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. If you are that person but you've lost your verification email, you can <a href="%(login_url)s">log in</a> and resend it. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out In case it doesn't: Include a note Invalid User name or email address. Invalid file given for media type. Is there another way to manage featured media? Last 10 successful uploads License License preference Location Log in Log in to create an account! Log out Logging in failed! Mark all read Max file size: {0} mb Media in-processing Media processing panel Media tagged with: %(tag_name)s MediaGoblin logo MetaData Metadata Metadata for "%(media_name)s" Most recent media Must provide an oauth_token. Name Name of user these media entries belong to New comments New email address New password No No OpenID service was found for %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. No failed entries! No media in-processing No open reports found. No processed entries, yet! No request token found. No users found. Nothing is currently featured. OAuth client connections Object Height Offender Old link found for "%s"; removing.
 Old password Older â Oops! Oops, your comment was empty. OpenID OpenID was successfully removed. OpenID's Operation not allowed Or login with OpenID! Or login with Persona! Or login with a password! Or register with OpenID! Or register with Persona! Original Original file PDF file Password Path to the csv file containing metadata information. Persona's Perspective Please check your entries and try again. Post new media as you Powered by <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, a <a href="http://gnu.org/">GNU</a> project. Primary Privilege Profile changes saved Promote RESOLVED Really delete %(title)s? Really delete collection: %(title)s? Really delete user '%(user_name)s' and all related media/comments? Really remove %(media_title)s from %(collection_title)s? Reason Reason for Reporting Recover password Redirect URI Released under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Source code</a> available. Remove Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Resend verification email Resent your verification email. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Save Save changes Secondary See your information (e.g profile, media, etc...) Send instructions Send the user a message Separate tags by commas. Set password Set your new password Side Sign in to create an account! Skipping "%s"; already set up.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Someone has registered an account with this username, but it still has to be activated. Sorry Dave, I can't let you do that!</p><p>You have tried  to perform a function that you are not allowed to. Have you been trying to delete all user accounts again? Sorry, I don't support that file type :( Sorry, a user with that email address already exists. Sorry, a user with that name already exists. Sorry, an account is already registered to that OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Sorry, authentication is disabled on this instance. Sorry, comments are disabled. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Sorry, registration is disabled on this instance. Sorry, reporting is disabled on this instance. Sorry, the OpenID server could not be found Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Sorry, this audio will not work because 
	your web browser does not support HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Sorry, this video will not work because
      your web browser does not support HTML5 
      video. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Stay logged in Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Tagged with Tags Take away privilege Terms of Service Tertiary That OpenID is not registered to this account. That Persona email address is not registered to this account. The Persona email address was successfully removed. The blog was not deleted because you have no rights. The client {0} has been registered! The collection was not deleted because you didn't check that you were sure. The item was not removed because you didn't check that you were sure. The media was not deleted because you didn't check that you were sure. The name of the OAuth client The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. The request sent to the server is invalid, please double check it The slug can't be empty The title can't be empty The title part of this collection's address. You usually don't need to change this. The title part of this media's address. You usually don't need to change this. The user id is incorrect. The verification key or user id is incorrect The verification key or user id is incorrect. There doesn't seem to be a page at this address. Sorry!</p><p>If you're sure the address is correct, maybe the page you're looking for has been moved or deleted. There doesn't seem to be any media here yet... These uploads failed to process: This address contains errors This field does not take email addresses. This field is required for public clients This field requires an email address. This is where your media will appear, but you don't seem to have added anything yet. This site is running <a href="http://mediagoblin.org">MediaGoblin</a>, an extraordinarily great piece of media hosting software. This user hasn't filled in their profile (yet). This will be visible to users allowing your
                application to authenticate as them. This will be your default license on upload forms. Title To add your own media, place comments, and more, you can log in with your MediaGoblin account. Top Type UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Username Username or Email Username or email Value Verification cancelled Verification of %s failed: %s Verify your email! Video transcoding failed View View all of %(username)s's media View most recent media View on <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL WebM file (VP8/Vorbis) WebM file (Vorbis codec) Website What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Woohoo! Submitted! Woohoo! edited blogpost is submitted Wrong password Yes Yes, really delete my account You added the attachment %s! You already have a collection called "%s"! You are Banned. You are about to delete an item from another user's collection. Proceed with caution. You are about to delete another user's Blog. Proceed with caution. You are about to delete another user's collection. Proceed with caution. You are about to delete another user's media. Proceed with caution. You are editing a user's profile. Proceed with caution. You are editing another user's collection. Proceed with caution. You are editing another user's media. Proceed with caution. You are logged in as You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! You can get a modern web browser that 
      can play this video at <a href="http://getfirefox.com">
      http://getfirefox.com</a>! You can now log in using your new password. You can only edit your own profile. You can track the state of media being processed for your gallery here. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> for formatting. You can use <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> for formatting. You can't delete your only OpenID URL unless you have a password set You can't delete your only Persona email address unless you have a password set. You cannot take action against an administrator You deleted the Blog. You deleted the collection "%s" You deleted the item from the collection. You deleted the media. You have been banned You have to select or add a collection You must be logged in so we know who to send the email to! You must provide a file. You need to confirm the deletion of your account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the You've already verified your email address! Your OAuth clients Your OpenID url was saved successfully. Your Persona email address was saved successfully. Your comment has been posted! Your email address has been verified. Your email address has been verified. You may now login, edit your profile, and submit images! Your last 10 successful uploads Your password was changed successfully an unknown application commented on your post day feature management panel. feed icon hour indefinitely log out minute month newer older unoconv failing to run, check log file until %(until_when)s week year {files_uploaded} out of {files_attempted} files successfully submitted â Newer â Blog post by <a href="%(user_url)s">%(username)s</a> â Browsing media by <a href="%(user_url)s">%(username)s</a> Project-Id-Version: GNU MediaGoblin
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-07-10 12:32-0500
PO-Revision-Date: 2014-07-10 17:32+0000
Last-Translator: cwebber <cwebber@dustycloud.org>
Language-Team: Hebrew (http://www.transifex.com/projects/p/mediagoblin/language/he/)
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 
                ××××× ×ª×××× ××¡×¤×¨ %(report_id)s
               
                ××××× ×××× ××¡×¤×¨ %(report_id)s
               
              ××××× ×¡×××¨ ××¡×¤×¨ %(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            â ×¤××¨×¡×× ×¢× ××× <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          ×ª××× ×××ª
            <a href="%(user_url)s"> %(user_name)s</a>
          × ×××§
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        â ×××× ××××××ª ×©× <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    ××× ×××¤×©×¨××ª× ×××××§ ××××××× ×¤×ª×××× ××©×¨ ××××× ×¢× ××× ××©×ª××©××.
   
    ××× ×××¤×©×¨××ª× ×××¤×© ××©×ª××©×× ××× ×× ×§×× ××¤×¢××××ª ×¢×× ×©×××ª ×¢××××.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   ×××©×ª ×× ×××©××× ×©××?  "%s" ××ª×××¡×¤× ×× ××××¡×£ "%s" "%s" ×××¨ ×§××× ××××¡×£ "%s" ××¡×¤×¨ ×ª×××××ª ×©×¤××¨×¡×× %(blog_owner_name)s's Blog %(collection_title)s (×××¡×£ ×©× %(username)s) %(collection_title)s ×××ª <a href="%(user_url)s">%(username)s</a> ××¤× × %(formatted_time)s %(username)s's Privileges ××××¡×¤×× ×©× %(username)s ××××× ×©× %(username)s ×××××§× ×©× %(username)s (××¡×¨) + -- ×××¨ -- -----------{display_type}-Features---------------------------
 ×××©××× ×©× <a href="%(user_url)s">%(user_name)s</a> ××××¡×¤×× ×©× <a href="%(user_url)s">%(username)s</a> ××××× ×©× <a href="%(user_url)s">%(username)s</a> ×××× ××©×ª××© <a href="%(user_url)s">%(username)s</a> ×¢× ×ª×××ª <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>×©××××ª ×××××</h2> <strong>×¡×××</strong> - ×××§×× ××××
                ×××¦××¨ ××§×©××ª ×× ×©×¨×ª GNU MediaGoblin ×©×× ××××××ª ××××××
                ×¢× ××× user agent (×××©× ××§×× server-side).<br />
                <strong>×¤××××</strong> - ×××§×× ×× ×××× ×××¦××¨ ××§×©××ª
                ×¡×××××ª ×× ×©× GNU MediaGoblin (×××©× ××§××
                â«JavaScript ××ª××¤×¢× client-side). ×××¡×£ ×¢× ××©××¤××ª ×× ×××¨ ×§××× ×¢×××¨ ××©×ª××© ××. ××××¨××ª ××©××× × ×©××¨× ×¤×¢××× ×©× × ×§×× ××××××× ×¤×¢×××× ×©× ×©××× Active Reports on %(username)s ××©×ª××©×× ×¤×¢×××× ×××¡×£ Add Blog Post ×××¡×£ ××ª×××ª ××××´× ×××©×××ª ×××¡×£ ×××¡×£ ×××¡×£ ×ª×××× ×××¡×£ ×××¡×£ ×××© ×××¡×£ OpenID ×××¡×¤×ª ×ª×¦×¨××£ ×××¡×¤×ª ×××× Add new Row ×××¡×£ ××ª ×ª×××× ×× ×××¡×£ ×× ×××¡×£ ×××¡×¤×ª ××××× ×©×× ×××¡×£ ××ª â%(media_title)sâ ×× ×××¡×£ ××ª×××¡×¤× All reports on %(username)s All reports that %(username)s has filed ×× ×××××××ª ×©×××¨××ª ××ª×¨ ×××¢×ª ×¡×××× ×! ××©××× × ×¢×××× ×¦×¨×× ××¢×××¨ ××§××××¦××. ×§×××¥ ×¢×××× ×§××× ×× × ××¦× ××××´× × ×©×× ××¦××¨××£ ×××¨×××ª ×× ×××¢ ××××¦× × ××ª× ××©× ××ª ××ª ×¡××¡××ª×. ××××´× ×¦×¤×× ×××××¢ ××¢×× ××¡×¤×¨ ×¨××¢×× ××¦××¨××£ ×××¨×××ª ×× ×××¢ ××××¦× ××¢×©××ª ××. ×¨×©××× ×¢× ××©××¤××ª ×× ×××¨ ×§××××ª ×¢×××¨ ××©×ª××© ××. ×××¨×¢× ×©×××× ××¤×××§×¦×××ª ×¢× ×××©× ×× ×××©××× ×©×× ××¡×××××ª:  ×¢×¨××¥ Atom ×ª×¦×¨××¤×× ××¨×©×× ××¨×©×× ×××©××× ××¨×©×× ××¡×ª×××× ××©×¨ BANNED until %(expiration_date)s ××§×©× ×¨×¢× Ban User ××¡××¨ ××ª ×××©×ª××© Banned Indefinitely ××× ××¤×××£ ××××¡×¤×× CAUTION: ×¢×××××ª CSRF ×× × ××××ª. ×× ×§×¨×× ×××××× × ×××¢ ××©×× ×××¡× ×¢××××× ×× ××©×× ××¡×× ××.<br/>×××× ×§×××¢× ×©× ×¢×××××ª ×¢×××¨ ×ª××× ××. ××××× ×× × ××ª× ××§×©×¨ ×× ×××××... ×× ×××××¨ ×××××
 ×©× × ××××¨××ª ××©××× ×©× × ××ª ×××××¢ ×©×× ×©× × ××ª ××¡××¡×× ×©××. ×©×× ×× ××××¨××ª ××©××× ×¢×××¨ %(username)s ×©×× ×× ××××´× ×©× %(username)s ××©× × ××¢×ª ××ª ××¡××¡×× ×©× %(username)s' Clear empty Rows ××××××× ×¡×××¨×× × ××¡×¤× ××ª×× ×××¡×£ ×××¡×£ "%s" ××ª×××¡×£! ×ª×××× ×ª×¦×××ª ×ª×××× ××¢×ª×§ ×××××§ ××ª ××××¢ ×× ×× ×ª×× ×××§×× ×©××: ××¢×ª×§× ×× ×××¡×× ×¤×××× × ××©××. ×× ×××ª× ××¤×©×¨××ª ××§×©×¨ ××ª "%s": %s ×§××× ×××× × ×§××©××¨ ×¡××× (symlink)
 ×× ××× × ××ª× ××§×¨×× ××ª ×§×××¥ ××ª××× ×. ×× ××× × ××ª× ××©××× ××××´× ××©××××¨ ×¡××¡×× ××××¨ ××©× ×××©×ª××© ×©×× ××× × ×¤×¢×× ×× ×©××ª×××ª ×××××´× ×©× ××©××× × ×× ××××ª×. ×× ××× × ××ª× ×××¦×× ×××©×× ×¢× ×©× ××©×ª××© ××. ×¦××¨ Create a Blog ××¦××¨×ª ××©×××! ×¦××¨ ×××¡×£ ×××© ×¦××¨ ××©××× ×××! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. × ××¦×¨ ×××§ Delete Blog ×××§ ××ª×××ª ××××´× ×××©×××ª ×××§ OpenID Delete collection %(collection_title)s ×××§ ××ª ×××©××× ×©×× ×××§ ××¦×××ª××ª ×××§ ××ª ××ª××× Demote ××¡××¨ ×ª××××¨ Description of Report ×ª××××¨ ×××¡×£ ×× ×ª××××¨ ×©× ××××× ×× ××× ××¨×¦×× × ×××ª××¨  ××× ×× ××©××× ×¢××××? ××× ××¨×©××ª× ××©××× ×¢××××? ×× ×§×! Don't process eagerly, pass off to celery ×××¨× ×××¨× ×××× ×¢×¨×× Edit Blog Edit Metadata ×¢×¨×× ××××§× ×¢×¨×××ª %(collection_title)s ×¢×¨×× %(media_title)s ×¢×¨×××ª ××××§× ×¢×××¨ %(username)s ×¢×¨×××ª ×ª×¦×¨××¤×× ×¢×××¨ %(media_title)s ××××´× ××ª×××ª ××××´× ×©×× ×× ××××´× ×××©×¨ ×××¨×× ×××××× ×¢× ××××× ×©×× × ××¨×© ×××××ª ××××´× Enable insite notifications about events. Enter the URL for the media to be featured ××× ××ª ×¡××¡××ª× ×××©× × ××× ×××××× ×©××ª× ×××¢××× ×©× ××©××× ××. ××× ××ª ××¡××¡×× ×©×× ××× ×××××× ×× ××ª× ×××¢××× ×©× ××©××× ××. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. ×××§××¨ FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel ×§×××¥ ×¤××¨×× ×§×××¥ ××× ××××× File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> ×©×××ª ××ª ×¡××¡××ª×? ××¤× ×× ××¢××¨ ×× ×¢×××: Granted ××× ×××¤×©×¨××ª× ××¢×§×× ×××¨ ×××¦× ×©× ××××× ×©××ª×¢×××ª ××©×¨×ª ××. ×× × ××§×× ××××¨ ××××¨×× ×××××ª×××. ×©××× %(username)s,

×××× ×××¤×¢×× ××ª ××©××× × ××©××¨××ª GNU MediaGoblin, ×¢××× ××¤×ª×× ××ª ×××ª×××ª ××××
××ª×× ××¤××¤× ××¨×©×ª ×©××:

%(verification_url)s ×©××× %(username)s,
%(comment_author)s ××××/× ×¢× ×¤×¨×¡××× (%(comment_url)s) ××¦× %(instance_name)s
 ×©×××, ××¨×× ×××× ×× ××ª×¨ MediaGoblin ××! ×©×××,

××¨×¦×× × × ××××ª ×× ××× × %(username)s. ×× ××× ×××, ×¢××× 
××¢×§×× ×××¨ ××§××©××¨ ××× ××× ××××ª ××ª ××ª×××ª ×××××´× ×©××.

%(verification_url)s

×× ××× × %(username)s ×× ×× ×××§×©×ª ×©×× ×× ××××´×, ××¤×©×¨ ×××ª×¢××
×××××¢×ª ××××´× ××. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? ××× ×××¤×, ×§××©××¨ ××××¨ symlink × ××¦×; ×××¡×¨.
 ×× × ×××× ×©××¨×¦×× × ×××××§ ×××ª ×× × ×××× ×©××¨×¦×× × ×××¡××¨ ××ª ×¤×¨×× ×× ×× ××××¡×£ ×××× Identifier ××××× ×××ª×××ª ×××××´× ××× (×ª××× ×¨××©×××ª!) ×¨×©××× ××××´× × ×©×× ×¢× ×××¨×××ª ×× ×××¢ ××××¦× ××©× ××ª ××ª ×¡××¡××ª×. ×× ××ª/× ××× ××× ×× ×××× ×××××ª ××ª ××××´× ××××××ª ×©××, ×××¤×©×¨××ª× <a href="%(login_url)s">×××ª×××¨</a> ×××©×××× ××××©. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says ×ª××× × ×¢×××¨ %(media_title)s ×ª××× × ×©× ×××××× ××ª×××¥ ××ª×¨ ×¢× ××××× ××××× ××× ××: ×××××ª ×¤×ª×§ ×©× ××©×ª××© ×× ××××´× ×©×××××. × ××ª× ×§×××¥ ×©××× ×¢×××¨ ×××¤××¡ ××××. Is there another way to manage featured media? 10 ××¢××××ª ×××¦××××ª ×××¨×× ××ª ×¨×©××× ×¢×××¤××ª ×¨×©××× ×××§×× ××ª×××¨××ª ××ª×××¨ ××× ×××¦××¨ ××©×××! ××ª× ×ª×§××ª ××ª×××¨××ª × ××©××! ×¡×× ××× ×× ×§×¨× ×××× ×§×××¥ ×××¨×× {0} ××´× ×××× ××××¦×¢-×¢×××× ××× ×¢×××× ×××× ×××× ××ª××××ª ×¢×: %(tag_name)s ×××× MediaGoblin MetaData Metadata Metadata for "%(media_name)s" ××××××ª ××××¨×× ××ª ××××ª×¨ ××© ××¡×¤×§ oauth_token. ×©× Name of user these media entries belong to ×ª×××××ª ×××©××ª ××ª×××ª ××××´× ×××©× ×¡××¡×× ×××©× No ×× × ××¦× ×©××¨××ª OpenID ×¢×××¨ %s No active reports filed on %(username)s ××× ××××¨ × ××¡ ×¢×××¨ ××××× ××
 ×× × ××¦×× ××××××× ×¡×××¨××. ××× ×¨×©××××ª ×××©×××ª! ××× ×××× ××××¦×¢-×¢×××× ×× × ××¦×× ××××××× ×¡×××¨××. ××× ×¨××©×××× ××¢×××××, ×¢××××! ×× × ××¦×× ×××ª ××§×©×. ×× × ××¦×× ××©×ª××©××. Nothing is currently featured. ×××××¨× ××§×× OAuth ×××× ××××××§× ××¢××× ×§××©××¨ ××©× × ××¦× ×¢×××¨ "%s"; ××¡××¨ ××¢×ª.
 ×¡××¡×× ××©× × â ××©× ×××ª×¨ ×××¤×¡! ×××¤×¡, ×ª××××ª× ×××ª× ×¨××§×. OpenID OpenID ×××¡×¨ ×××¦×××. OpenID's ×¤×¢××× ×× ×××¨×©××ª ×× ××ª×××¨ ×¢× OpenID! ×× ×ª×ª×××¨ ×¢× ×××©×××ª! ×× ××ª×××¨ ××¢××¨×ª ×¡××¡××! ×× ×××¨×©× ××¢××¨×ª OpenID! ×× ×¨×©×× ×××©×××ª ×××©×! ××§××¨××ª ×§×××¥ ××§××¨× ×§×××¥ PDF ×¡××¡×× Path to the csv file containing metadata information. Persona's × ×§×××ª ××× ×× × ××××§ ××ª ×¨×©××××ª×× ×× ×¡× ×©××. Post new media as you ×××× ×¢ ×¢× ××× <a href="http://mediagoblin.org/" title='×××¨×¡× %(version)s'>MediaGoblin</a>, ×¤×¨×××§× <a href="http://gnu.org/">GNU</a>. Primary Privilege ×©×× ××× ××××§× × ×©××¨× Promote × ×¤×ª×¨ ××××ª ×××××§ ××ª %(title)s? Really delete collection: %(title)s? ××××ª ×××××§ ××ª ×××©×ª××© '%(user_name)s' ×××ª ×× ×××××/××ª×××××ª ××§×©××¨××ª? ××××ª ×××¡××¨ ××ª %(media_title)s ×× %(collection_title)s? ×¡××× ×¡×××ª ××××× ×©××××¨ ×¡××¡×× Redirect URI ××©×××¨×¨ ×ª××ª ××¨×©××× <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">×§×× ××§××¨</a> ××××. ××¡×¨ Remove %(media_title)s from %(collection_title)s ×××× Report #%(report_number)s Report ID ××× ××¨××× ××××××× ×××× ×¢× ×××× ××× ××××××× ×××× ×¢× ××× Reported Comment Reported Content Reported Media Entry ×ª×××× ××××××ª ××××× ×¢× ×ª×××× ×× ××××× ×¢× ×¢×¨× ×××× ×× ×©×× ××××´× ×××××ª ×©×× ×©×× ××ª ××××´× ××××××ª ×©××. ×¤×ª××¨ ×¤×ª××¨ ××ª ××××× ×× × ×¤×ª×¨ ××××¨ ×× ××× ××××××× Return to Users Panel ×©×××¨ ×©×××¨ ×©×× ×××× Secondary ×¨×× ××ª ×××××¢ ×©×× (×××©× ××××§×, ××××, ××××³...) ×©×× ×××¨×××ª ×©×× ×× ×××©×ª××© ××××¢× ××¤×¨× ×ª××××ª ××¢××¨×ª ×¤×¡××§××. ××××¨ ×¡××¡×× ××××¨ ××ª ×¡××¡××ª× ××××©× ×¦× ××ª×××¨ ××× ×××¦××¨ ××©×××! ×××× ×¢× "%s"; ×××¨ ×××××¨.
 ××©××¤××ª Someone has registered an account with this username, but it still has
        to be activated. ×××©×× ×¨×©× ××©××× ×¢× ×©× ××©×ª××© ××, ×× ×¢××× ××¢×××¨ ××§××××¦××. ×¦×¨ ×× ××××, ×× × ×× ×××× ×××ª××¨ ×× ××¢×©××ª ×××ª!</p><p>× ××¡××ª ×××¦×¢ ×¤×¢××× ×©××× × ×××¨×©× ××¢×©××ª. ××× × ××¡××ª ×××××§ ××ª ×× ×××©××× ××ª ×©× ×××©×ª××©×× ×©××? ××¦×¢×¨× ×, ××× × × ×ª××× ××××¤××¡ ×§×××¥ ×× :( ××¦×¢×¨× ×, ××©×ª××© ×¢× ××ª×××ª ××××´× ×× ×××¨ ×§×××. ××¦×¢×¨× ×, ××©×ª××© ×¢× ×©× ×× ×××¨ ×§×××. ××¦×¢×¨× ×, ×§××× ×××¨ ××©××× ××©×¨ ×¨×©×× ×¢× OpenID. ××¦×¢×¨× ×, ×§××× ×××¨ ××©××× ××©×¨ ×¨×©×× ××××©×××ª ××××´× ××. ××¦×¢×¨× ×, ×§××× ×××¨ ××©××× ××©×¨ ×¨×©×× ×¢× ×××©×××ª ××ª×××ª ××××´× ××. ××¦×¢×¨× ×, ×××××ª ××× × ×× ×××¨× ×¢× ×©×¨×ª ××. ××¦××¢×¨××, ×ª×××××ª ×× ×××¨×××ª. ××¦×¢×¨× ×, ×× × ××¦× ××××× ×××. Sorry, no such user found. Sorry, no user by username '{username}' exists ××¦×¢×¨× ×, ×¨××©×× ××× × ×× ×××¨× ×¢× ×©×¨×ª ××. ××¦×¢×¨× ×, ××××× ××× × ×× ×××¨××× ×¢× ×©×¨×ª ××. ××¦×¢×¨× ×, ×©×¨×ª OpenID ×× ××× ×××× ×××××¦× ××¦×¢×¨× ×, ×××× ××§×××¥ ×××× ×××. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. ××¦×¢×¨× ×, ××××× ×× ×× ××¢××× ×××××× 
	×©××¤××¤× ××¨×©×ª ×©×× ×× ×ª××× 
	××××× ×©× HTML5. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. ××¦×¢×¨× ×, ××××× ×× ×× ××¢××× ×××××× 
      ×©××¤××¤× ××¨×©×ª ×©×× ×× ×ª××× 
      ××××× ×©× HTML5. ××¦×¢×¨× ×, ××¢×××ª ×§×××¥ ×× ×ª×©×× ×××ª× ×××¢×× ×× ×××× ×××¢××× ×©××. ××¦×¢×¨× ×, ××¦××ª ××ª ×××××ª ×××¢×××. ×¡××××¡ ×××©××¨ ×××××¨ ×××¨×©× ×× ×ª×××××ª ××ª×× %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. ××ª××××ª ×¢× ×ª××××ª ××¡×¨ ×¤×¨××××××× ×ª× ×× ×©××¨××ª Tertiary ××ª×××ª  ×× ×× ×¨×©××× ×××©××× ××. ××ª×××ª ××××´× ×××©×××ª ×× ×× ×¨×©××× ×××©××× ××. ××ª×××ª ××××´× ×××©×××ª ×××¡×¨× ×××¦×××. The blog was not deleted because you have no rights. ×××§×× {0} × ×¨×©×! ××××¡×£ ×× ×××¡×¨ ×××××× ×©×× ×¡××× ×ª ×©××ª× ××××. ××¤×¨×× ×× ×××¡×¨ ×××××× ×©×× ×¡××× ×ª ×©××ª× ××××. ××××× ×× × ×××§× ×××××× ×©×× ×¡××× ×ª ×©××ª× ××××. ××©× ×©× ××§×× OAuth The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. ×××§×©× ××©×¨ × ×©××× ×× ××©×¨×ª ××× × ×©××××, ×× × ××× ×××ª× ×××× ×××©××¤××ª ×× ××××× ×××××ª ×¨××§× ××××ª×¨×ª ×× ××××× ×××××ª ×¨××§× ××××¨ ××××ª×¨×ª ×©× ××ª×××ª ×××¡×£ ××. ××¨×× ××× ×××¨× ××©× ××ª ××ª ×××§ ××. ××××¨ ××××ª×¨×ª ×©× ××ª×××ª ×××× ××. ××¨×× ××× ×××¨× ××©× ××ª ××ª ×××§ ××. ×××× ×××©×ª××© ××× × ×××××§. ××¤×ª× ××××××ª ×× ××××ª ××©×ª××© ××× × ×©××××× ××¤×ª× ××××××ª ×× ×××× ×××©×ª××© ××× × ×××××§. ×× × ×¨×× ×× ×§××× ×¢××× ×××ª×××ª ××. ×¦×¨ ××!</p><p>×× ××ª× ×××× ×©×××ª×××ª ××× × ×××××§×ª, ×××ª×× ×©××¢××× ×©××ª× ×××¤×© ××¢×ª ×××¢××¨ ×× × ×××§. ×× × ×¨×× ×©××© ××× ×××× ×××©×× ×¢××××... ××¢××××ª ××× × ××©×× ×××ª×¢××: ××ª×××ª ×× ××××× ×©×××××ª ×©×× ×× ×× ×××§× ××ª××××ª ××××´×. ×©×× ×× ××× × ××¨××© ×¢×××¨ ××§××××ª ×¤×××××× ×©×× ×× ××¦×¨×× ××ª×××ª ××××´×. ××× ×× ×××§×× ×× ××××× ×©×× ×ª××¤××¢, ×××× ×× × ×¨×× ×©×××¡×¤×ª ××©×× ×¢××××. ××ª×¨ ×× ××¨××¥ <a href="http://mediagoblin.org">MediaGoblin</a>, ××ª×××ª ×ª××× ×ª ×××¨×× ×××× ×××¦××ª ×× ××××. ××©×ª××© ×× ×× ×××× ××××§× (×¢××××). ×× ××¨×× ×××©×ª××©×× ×©××ª××¨××
                ××××©×××× ×©×× ××××ª ×××ª×. ×× ×××× ××¨×©×××× ×××©×ª×× (××¨××¨×ª ××××) ×©×× ××××¤×¡× ××¢×××. ×××ª×¨×ª ×××× ××××¡××£ ××ª ××××× ×©××, ××¤×¨×¡× ×ª×××××ª, ××¢××, ×××¤×©×¨××ª× ×××ª×××¨ ×¢× ××©××× MediaGoblin. ×¨××© ×××¤××¡ UnBan User Unfeature Update Metadata ××× × ×××× ××©×ª××© ××× ××©×ª××© ××©×ª××© ×××¡×¨ ×¢×: User: %(username)s ×©× ××©×ª××© ×©× ××©×ª××© ×× ××××´× ×©× ××©×ª××© ×× ××××´× Value ×××××ª ×××× ×××××ª ×©× %s × ××©×: %s ×××ª ××ª ×××××´× ×©××! ×××¨×ª ××××× × ××©×× View ×¦×¤×× ××× ××××× ×©× %(username)s View most recent media ××¦× ××ª×× <a href="%(osm_url)s">OpenStreetMap</a> ××××¨× ×××ª WebGL ×§×××¥ WebM &rlm;(VP8/Vorbis) ×§×××¥ WebM (×§×××§ Vorbis) ××ª×¨ ×¨×©×ª ××××× ×¤×¢××× ×ª× ×§×× ××× ××¤×ª××¨ ××ª ××××× ××? What is a Primary Feature? What is a Secondary Feature? ×××× ×¤×¨××××××××ª ××ª× ×ª×¡××¨? ××ª× ××¦××¨×£ ××ª× ×××× ××××¢ ××ª× ×××¡×¨ ××ª ××©×ª××© ××? ××××! × ×©××! Woohoo! edited blogpost is submitted ×¡××¡×× ×©×××× Yes ××, ××××ª ×××××§ ××ª ××©××× × ×××¡×¤×ª ××ª ××ª×¦×¨××£ %s! ×××¨ ××© ×× ×××¡×£ ×©×§×¨×× ××©× "%s"! ××ª× ××¡××. ×××¨×ª ×××××§ ×¤×¨×× ×× ×××¡×£ ×©× ××©×ª××© ×××¨. ×××©× ×××××¨××ª. You are about to delete another user's Blog. Proceed with caution. ×××¨×ª ×××××§ ×××¡×£ ×©× ××©×ª××© ×××¨. ×××©× ×××××¨××ª. ×××¨×ª ×××××§ ×××× ×©× ××©×ª××© ×××¨. ×××©× ×××××¨××ª. ××ª× ×¢××¨× ××××§× ×©× ××©×ª××©. ×××©× ×××××¨××ª. ××ª× ×¢××¨× ×××¡×£ ×©× ××©×ª××© ×××¨. ×××©× ×××××¨××ª. ××ª× ×¢××¨× ×××× ×©× ××©×ª××© ×××¨. ×××©× ×××××¨××ª. ××ª× ×××××¨ ××ª××¨ ××× × ××©×ª××© ×¤×¢×× ×¢××. ×× × ×¦××¨ ×§×©×¨ ×¢× ×× ×× ×××¢×¨××ª ××× ××ª×¤×¢× ××××© ××ª ××©××× ×. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! ×××¤×©×¨××ª× ×××©×× ××¤××¤× ×¨×©×ª ××××¨× × 
	×©×× ××¡××× ×× ×× ××ª ××××× ×× ×××ª×¨ <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! ×××¤×©×¨××ª× ×××©×× ××¤××¤× ×¨×©×ª ××××¨× × 
      ×©×× ××¡××× ×× ×× ××ª ××××× ×× ×××ª×¨ <a href="http://getfirefox.com">
      http://getfirefox.com</a>! ××¢×ª ×××¤×©×¨××ª× ×××ª×××¨ ××××¦×¢××ª ×¡××¡××ª× ××××©×. ×××¤×©×¨××ª× ××¢×¨×× ×¨×§ ××ª ×××××§× ×©××. ×××¤×©×¨××ª× ××¢×§×× ×××¨ ××¦× ×©× ×××× ×©××¦××× ××ª×××× ×¢×××× ×¢×××¨ ××××¨×× ×©×× ×××. ×××¤×©×¨××ª× ×××©×ª××© ××ª××××¨
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> ×¢×××¨ ×¢××¦××. ×××¤×©×¨××ª× ×××©×ª××© ××ª××××¨
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> ×¢×××¨ ×¢××¦××. ×××¤×©×¨××ª× ×××©×ª××© ××ª××××¨
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> ××¢××¦××. ×××¤×©×¨××ª× ×××©×ª××© ××ª××××¨ <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> ×¢×××¨ ×¢××¦××. ××× ×××¤×©×¨××ª× ×××××§ ××ª×××ª  ×××©×¨ ×× ×××××× ×©×× ××× ×× ×× ××××¨×ª ×¡××¡××. ××× ×××¤×©×¨××ª× ×××××§ ××ª×××ª ××××´× ×××©×××ª ×××©×¨ ×× ×××××× ×©×× ××× ×× ×× ××××¨×ª ×¡××¡××. ××× ×××¤×©×¨××ª× ×× ×§×× ××××¦×¢×× ×× ×× ××× ××× You deleted the Blog. ×××§×ª ××ª ××××¡×£ "%s" ×××§×ª ××ª ××¤×¨×× ×× ×××¡×£ ××. ×××§×ª ××ª ×××× ××. × ××¡×¨×ª ×¢××× ×××××¨ ×× ××××¡××£ ×××¡×£ ×¢××× ×××ª×××¨ ×¢× ×× ×ª ×©× ××¢ ×× ×× ××©××× ××ª ×××××´×! ×¢××× ××¡×¤×§ ×§×××¥. ×¢××× ××××ª ××ª ×××××§× ×©× ××©××× ×. ××ª× ×× ×ª×§×× ××ª×¨×××ª ×¢×××¨ ××××¢××ª ×¢× %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the ×××¨ ××××ª×ª ××ª ××ª×××ª ×××××´× ×©××! ××§××××ª OAuth ×©×× ××ª×××ª OpenID × ×©××¨× ×××¦×××. ××ª×××ª ××××´× ×××©×××ª × ×©××¨× ×××¦×××. ×ª××××ª× ×¤××¨×¡××! ××ª×××ª ×××××´× ×©×× ××××ª×. ××ª×××ª ×××××´× ×©×× ××××ª×. ××¢×ª ×××¤×©×¨××ª× ×××ª×××¨, ××¢×¨×× ××ª ××××§× ×, ×××©××× ×ª××× ××ª! 10 ×××¢××××ª ××××¦××××ª ×©×× ×¡××¡××ª× ×©×× ×ª× ×××¦××× ×××¤×××§×¦×× ×× ××××¨×ª ××××/× ×¢× ×¤×¨×¡××× ××× feature management panel. ×¦××××ª ×¢×¨××¥ ×©×¢× ××¦×××ª× ××ª× ×ª×§××ª ××§× ××××© ×××© ×××ª×¨ ××©× ×××ª×¨ unoconv × ××©× ××¤×¢××, ××××§ ×§×××¥ ×××× ×¢× %(until_when)s ×©×××¢ ×©× × {files_uploaded} out of {files_attempted} files successfully submitted ×××© ×××ª×¨ â â Blog post by <a href="%(user_url)s">%(username)s</a> â ××¤×××£ ××××× ×©× <a href="%(user_url)s">%(username)s</a> 