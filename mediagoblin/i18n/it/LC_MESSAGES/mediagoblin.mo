��    �     $              ,  =   -  ;   k  8   �  z   �  ?   [  �   �  o      A  �   L   �$    %  �  +'  {   )  �  �)  �   {+  H   m,  M   �,     -     -     %-     7-     I-     Z-     n-     �-     �-     �-     �-  0   �-  ?   '.     g.     ~.     �.     �.     �.     �.     �.     �.  >   �.  2   6/  5   i/  /   �/  Z   �/     *0     E0  �  \0  9   �1     2     52     B2     W2     v2     �2     �2     �2     �2     �2     �2     �2     �2  	   3     3     3     )3     =3  )   L3     v3     |3  '   �3     �3     �3  6   �3  +   4  H   =4  J   �4  5   �4     5  .   5  	   G5     Q5     ]5     k5     �5  	   �5      �5     �5     �5     �5     �5     �5     �5     6  �   6     �6  "   �6     �6     �6     7  (   $7     M7      k7     �7     �7     �7  
   �7     �7     �7     �7  %   �7  !   8  4   ;8     p8  z   �8  )   
9     49     ;9     I9     \9     r9     �9     �9     �9     �9     �9     �9     �9     :  &   :     ::     L:     _:     r:     y:     ~:     �:     �:     �:     �:     �:     ;  )   ,;     V;     _;     n;  	   s;     };     �;     �;     �;     �;  '   �;     <     <  (   (<     Q<  )   k<  *   �<  6   �<  2   �<  Z   *=     �=  S   �=  ?   �=  <   !>  4   ^>     �>     �>     �>     �>     �>  .   �>  �   ?     �?     �?     @     @  G   @  ,   a@  }   �@  b   A  +   oA  �   �A     �B  O   �B     �B  "   C  4   7C     lC  8   �C     �C  
   �C  ~   �C  y   RD     �D     LE     fE     �E     �E  #   �E  "   �E  .   �E     F     8F     @F     SF     \F     cF     �F     �F     �F     �F     �F     �F     �F     
G     G     $G     -G     KG     ]G     zG  *   G     �G     �G     �G     �G  "   �G  '   �G  "   $H     GH     `H     sH     �H     �H     �H     �H     �H     I     I     *I  #   3I     WI  	   dI     nI     tI     �I      �I     �I     �I     �I     �I     J      J     9J     SJ     \J     jJ     sJ  5   |J  	   �J     �J  (   �J     �J  �   K     �K  	   �K     �K     �K     �K     �K  $   �K  B   L  8   HL     �L     �L     �L     �L  �   �L     KM  0   RM     �M     �M  	   �M     �M     �M     �M     �M     �M     �M     N     #N     4N     KN     fN     �N     �N     �N     �N     �N     �N     �N     �N  	   O  1   O     AO     SO     kO     �O     �O     �O     �O     �O     �O  _   �O  W   OP  �   �P  (   MQ  5   vQ  ,   �Q  7   �Q  >   R  H   PR  3   �R     �R     �R     S  .   #S  1   RS  .   �S  +   �S      �S  X    T  Z   YT  k   �T  c    U  ?   �U  *   �U     �U     �U     V  �   #V     �V     �V     �V     �V     �V  .   �V  =   W  3   UW  4   �W  #   �W  K   �W  E   .X  F   tX     �X  n   �X  A   GY     �Y     �Y  S   �Y  N   Z     ]Z  ,   wZ  -   �Z  �   �Z  .   t[      �[     �[  )   �[  )   \  %   5\  T   [\  �   �\  /   1]  `   a]  2   �]     �]  ^   �]     Z^     ^^  
   c^  	   n^     x^     �^  
   �^     �^     �^     �^     �^     �^     _     
_     !_     ?_     R_     k_      p_     �_  /   �_     �_     �_     �_     `     `  0   #`  7   T`  #   �`     �`     �`     �`     �`  $   �`     !a     0a     4a     Ra  *   oa     �a  U   �a  B    b  H   Cb  C   �b  7   �b  @   c  ;   Ic     �c  ]   �c  {   �c  |   td  �   �d  �   e  +   f  #   1f  G   Uf  �   �f  �   4g  �   �g  u   jh  D   �h  P   %i  /   vi     �i     �i  )   �i     j     j  &   2j  :   Yj     �j  1   �j  6   �j  .  k  +   El     ql  '   �l  2   �l     �l  %   �l  ^   #m     �m  &   �m     �m     �m     �m     �m  	   n     n     $n     1n     9n     @n     Fn     Ln  &   Rn     yn     �n     �n  F   �n  	   �n  8   �n  =   "o  �  `o  '   q  1   Fq  %   xq  z   �q  ?   r  W   Yr  O   �r  A  s  L   Cw    �w  �  �y  {   �{  �  |  �   �}  H   �~  T   *          �     �     �     �     �     �      �      &�     G�     `�  /   {�  ?   ��     �     �     �  !   4�     V�  	   n�     x�     z�  >   ��  3   Ɂ  3   ��  <   1�  f   n�     Ղ     ��  �  �  3   ��      ̄     �     ��     �     3�     A�     J�  #   X�     |�     ��     ��     Å     օ     �     �     �     (�  !   @�  -   b�     ��     ��  '   ��     ݆     ��  9    �  +   :�  L   f�  G   ��  2   ��     .�  4   I�  	   ~�     ��     ��     ��     ��  	   Ј      ڈ     ��     �     �     (�  	   <�     F�     Z�  �   c�     �  9   �  #   U�     y�     ��  ;   ��  &   �  *   �     <�     M�     a�     m�     v�     ��     ��  &   ��  !   Ћ  4   �  %   '�  �   M�  ,   ׌     �     	�     �     (�     @�     P�     c�     |�     ��     ��     ��  "   ��     Ӎ  &   �     �     #�     ;�     P�     W�     \�     h�     ~�     ��     ��     ˎ  !   �  )   �     2�     :�     M�  	   V�     `�     n�  %   �      ��  +   Ə  0   �     #�     )�  O   9�  *   ��  +   ��  *   ��  T   �  U   `�  Z   ��     �  S   �  ?   m�  <   ��  4   �     �     '�     @�     E�     V�  .   c�  �   ��      z�  	   ��     ��     ��  Z   ��  /   �  �   I�  g   Ε  +   6�    b�     �  O   ��     �  "   ��  4   �      Q�  =   r�     ��  
   ��  ~   ��  o   =�     ��     -�     G�     e�     y�  )   ��  :   ��  .   �     �     =�     E�  	   W�     a�     h�     ��     ��     ��  #   ��  !   כ  *   ��     $�     C�     Z�     c�     l�     ��      ��     ʜ  *   Ϝ     ��     	�     �     .�  .   1�  '   `�  "   ��     ��     ĝ  (   �     �  !   "�     D�     \�     s�     ��     ��  	   ��  #   Ş     �     ��     �      �     2�  %   9�  
   _�     j�     ��     ��     ��     ʟ     �  	   ��     �     �      �  5   )�  	   _�     i�  /   u�  +   ��  �   Ѡ     ^�  	   f�     p�     ��     ��  !   ��  $   ¡  g   �  B   O�     ��     ��     ��     Ϣ  �   �     ��  0   ��     ��     £  	   ܣ  '   �     �     �     (�     5�     F�     W�     l�     �  &   ��     ��     ݤ     ��     �     �      #�     D�     Z�     `�  	   s�  N   }�     ̥     ݥ     ��     �     )�     C�      H�     i�     ��  _   ��  Y   �  �   J�  0   �  A   �  /   _�  @   ��  Q   Ш  Q   "�  @   t�  *   ��  $   �     �  .    �  >   O�  ?   ��  2   Ϊ  $   �  X   &�  o   �  k   �  o   [�  F   ˬ  :   �     M�     S�     c�  �   ��     �     �     �     ,�     @�  0   I�  B   z�  8   ��  4   ��  "   +�  O   N�  N   ��  K   ��     9�  p   O�  D   ��     �     %�  q   E�  g   ��     �  0   9�  1   j�  �   ��  <   T�  2   ��      ĳ  )   �  /   �  )   ?�  \   i�  �   ƴ  ;   V�  `   ��  K   �     ?�  p   F�     ��     ��  
   ö  	   ζ     ض  !   �     
�      �     ;�     N�     Z�     x�     ��     ��     ��     ʷ     �     ��  4   �     7�  5   N�     ��     ��     ��     ��     Ӹ  6   ܸ  7   �     K�     f�     r�  $   ��     ��  $   ��     ܹ     �  +   �     �  )   8�     b�  S   o�  B   ú  D   �  P   K�  A   ��  E   ޻  N   $�     s�  m   ��  {   �  �   o�  �   �  �   ��  4   �  $   R�  Q   w�  s   ɿ  ~   =�  �   ��  }   W�  R   ��  `   (�  '   ��     ��     ��  (   ��     �     &�  *   8�  F   c�     ��  /   ��  6   ��  .  +�  +   Z�     ��  0   ��  =   ��  "   	�  +   ,�  m   X�  %   ��  .   ��     �     7�     Q�     X�  	   r�     |�     ��     ��     ��     ��     ��     ��  &   ��     ��  	   ��     �  F   �     O�  8   `�  Q   ��   
                Comment Report #%(report_id)s
               
                Media Report #%(report_id)s
               
              Closed Report #%(report_id)s
             
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
            ❖ Published by <a href="%(user_url)s"
                          class="comment_authorlink">%(username)s</a>
       
          CONTENT BY
            <a href="%(user_url)s"> %(user_name)s</a>
          HAS BEEN DELETED
         
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
    Here you can look up users in order to take punitive actions on them.
   
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.   to access your account?  "%s" added to collection "%s" "%s" already in collection "%s" # of Comments Posted %(blog_owner_name)s's Blog %(collection_title)s (%(username)s's collection) %(collection_title)s by <a href="%(user_url)s">%(username)s</a> %(formatted_time)s ago %(username)s's Privileges %(username)s's collections %(username)s's media %(username)s's profile (remove) + -- Select -- -----------{display_type}-Features---------------------------
 <a href="%(user_url)s">%(user_name)s</a>'s account <a href="%(user_url)s">%(username)s</a>'s collections <a href="%(user_url)s">%(username)s</a>'s media <a href="%(user_url)s">%(username)s</a>'s media with tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>File a Report</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). A collection with that slug already exists for this user. Account settings saved Action Taken Active Reports Filed Active Reports on %(username)s Active Users Add Add Blog Post Add a Persona email address Add a collection Add a comment Add a new collection Add an OpenID Add attachment Add media Add new Row Add this comment Add to a collection Add your media Add “%(media_title)s” to a collection Added All reports on %(username)s All reports that %(username)s has filed All rights reserved Allow Almost done! Your account still needs to be activated. An acceptable processing file was not found An email has been sent with instructions on how to change your password. An email should arrive in a few moments with instructions on how to do so. An entry with that slug already exists for this user. An error occured Applications with access to your account can:  Atom feed Attachments Authorization Authorization Complete Authorization Finished Authorize BANNED until %(expiration_date)s Bad Request Ban User Ban the user Banned Indefinitely Bio Browse collections CAUTION: CSRF cookie not present. This is most likely the result of a cookie blocker or somesuch.<br/>Make sure to permit the settings of cookies for this domain. Cancel Cannot link theme... no theme set
 Change account settings Change your information Change your password. Changing %(username)s's account settings Changing %(username)s's email Changing %(username)s's password Clear empty Rows Closed Reports Collected in Collection Collection "%s" added! Comment Comment Preview Copy and paste this into your client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Could not read the image file. Could not send password recovery email as your username is inactive or your account's email address has not been verified. Couldn't find someone with that username. Create Create a Blog Create an account! Create new collection Create one here! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Created Delete Delete Blog Delete a Persona email address Delete an OpenID Delete collection %(collection_title)s Delete my account Delete permanently Delete the content Demote Deny Description Description of Report Description of this collection Description of this work Do you want to authorize  Don't have an account yet? Don't have one yet? It's easy! Don't process eagerly, pass off to celery Download Download model Edit Edit Blog Edit Metadata Edit profile Editing %(collection_title)s Editing %(media_title)s Editing %(username)s's profile Editing attachments for %(media_title)s Email Email address Email me when others comment on my media Email verification needed Enable insite notifications about events. Enter the URL for the media to be featured Enter your old password to prove you own this account. Enter your password to prove you own this account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Explore FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File File Format File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Forgot your password? Front Go to page: Granted Here you can track the state of media being processed on this instance. Here's a spot to tell others about yourself. Hi %(username)s,

to activate your GNU MediaGoblin account, open the following URL in
your web browser:

%(verification_url)s Hi %(username)s,
%(comment_author)s commented on your post (%(comment_url)s) at %(instance_name)s
 Hi there, welcome to this MediaGoblin site! Hi,

We wanted to verify that you are %(username)s. If this is the case, then 
please follow the link below to verify your new email address.

%(verification_url)s

If you are not %(username)s or didn't request an email change, you can ignore
this email. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 I am sure I want to delete this I am sure I want to remove this item from the collection ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. If you are that person but you've lost your verification email, you can <a href="%(login_url)s">log in</a> and resend it. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out In case it doesn't: Include a note Invalid User name or email address. Invalid file given for media type. Is there another way to manage featured media? Last 10 successful uploads License License preference Location Log in Log in to create an account! Log out Logging in failed! Mark all read Max file size: {0} mb Media in-processing Media processing panel Media tagged with: %(tag_name)s MediaGoblin logo MetaData Metadata Metadata for "%(media_name)s" Most recent media Must provide an oauth_token. Name Name of user these media entries belong to New comments New email address New password No No OpenID service was found for %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. No failed entries! No media in-processing No open reports found. No processed entries, yet! No request token found. No users found. Nothing is currently featured. OAuth client connections Object Height Offender Old link found for "%s"; removing.
 Old password Older → Oops! Oops, your comment was empty. OpenID OpenID was successfully removed. OpenID's Operation not allowed Or login with OpenID! Or login with Persona! Or login with a password! Or register with OpenID! Or register with Persona! Original Original file PDF file Password Path to the csv file containing metadata information. Persona's Perspective Please check your entries and try again. Post new media as you Powered by <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, a <a href="http://gnu.org/">GNU</a> project. Primary Privilege Profile changes saved Promote RESOLVED Really delete %(title)s? Really delete collection: %(title)s? Really delete user '%(user_name)s' and all related media/comments? Really remove %(media_title)s from %(collection_title)s? Reason Reason for Reporting Recover password Redirect URI Released under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Source code</a> available. Remove Remove %(media_title)s from %(collection_title)s Report Report #%(report_number)s Report ID Report management panel Report media Report panel Reported By Reported Comment Reported Content Reported Media Entry Reported comment Reporting this Comment Reporting this Media Entry Resend verification email Resent your verification email. Resolve Resolve This Report Resolved Return to Reports Panel Return to Users Panel Save Save changes Secondary See your information (e.g profile, media, etc...) Send instructions Send the user a message Separate tags by commas. Set password Set your new password Side Sign in to create an account! Skipping "%s"; already set up.
 Slug Someone has registered an account with this username, but it still has
        to be activated. Someone has registered an account with this username, but it still has to be activated. Sorry Dave, I can't let you do that!</p><p>You have tried  to perform a function that you are not allowed to. Have you been trying to delete all user accounts again? Sorry, I don't support that file type :( Sorry, a user with that email address already exists. Sorry, a user with that name already exists. Sorry, an account is already registered to that OpenID. Sorry, an account is already registered to that Persona email. Sorry, an account is already registered with that Persona email address. Sorry, authentication is disabled on this instance. Sorry, comments are disabled. Sorry, no such report found. Sorry, no such user found. Sorry, no user by username '{username}' exists Sorry, registration is disabled on this instance. Sorry, reporting is disabled on this instance. Sorry, the OpenID server could not be found Sorry, the file size is too big. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Sorry, this audio will not work because 
	your web browser does not support HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Sorry, this video will not work because
      your web browser does not support HTML5 
      video. Sorry, uploading this file will put you over your upload limit. Sorry, you have reached your upload limit. Status Stay logged in Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Tagged with Tags Take away privilege Terms of Service Tertiary That OpenID is not registered to this account. That Persona email address is not registered to this account. The Persona email address was successfully removed. The blog was not deleted because you have no rights. The client {0} has been registered! The collection was not deleted because you didn't check that you were sure. The item was not removed because you didn't check that you were sure. The media was not deleted because you didn't check that you were sure. The name of the OAuth client The redirect URI for the applications, this field
            is <strong>required</strong> for public clients. The request sent to the server is invalid, please double check it The slug can't be empty The title can't be empty The title part of this collection's address. You usually don't need to change this. The title part of this media's address. You usually don't need to change this. The user id is incorrect. The verification key or user id is incorrect The verification key or user id is incorrect. There doesn't seem to be a page at this address. Sorry!</p><p>If you're sure the address is correct, maybe the page you're looking for has been moved or deleted. There doesn't seem to be any media here yet... These uploads failed to process: This address contains errors This field does not take email addresses. This field is required for public clients This field requires an email address. This is where your media will appear, but you don't seem to have added anything yet. This site is running <a href="http://mediagoblin.org">MediaGoblin</a>, an extraordinarily great piece of media hosting software. This user hasn't filled in their profile (yet). This will be visible to users allowing your
                application to authenticate as them. This will be your default license on upload forms. Title To add your own media, place comments, and more, you can log in with your MediaGoblin account. Top Type UnBan User Unfeature Update Metadata User management panel User panel User will be banned until: User: %(username)s Username Username or Email Username or email Value Verification cancelled Verification of %s failed: %s Verify your email! Video transcoding failed View View all of %(username)s's media View most recent media View on <a href="%(osm_url)s">OpenStreetMap</a> Warning from WebGL WebM file (VP8/Vorbis) WebM file (Vorbis codec) Website What action will you take to resolve the report? What is a Primary Feature? What is a Secondary Feature? What privileges will you take away? When Joined When Reported Why are you banning this User? Woohoo! Submitted! Woohoo! edited blogpost is submitted Wrong password Yes Yes, really delete my account You added the attachment %s! You already have a collection called "%s"! You are Banned. You are about to delete an item from another user's collection. Proceed with caution. You are about to delete another user's Blog. Proceed with caution. You are about to delete another user's collection. Proceed with caution. You are about to delete another user's media. Proceed with caution. You are editing a user's profile. Proceed with caution. You are editing another user's collection. Proceed with caution. You are editing another user's media. Proceed with caution. You are logged in as You are no longer an active user. Please contact the system admin to reactivate your account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! You can get a modern web browser that 
      can play this video at <a href="http://getfirefox.com">
      http://getfirefox.com</a>! You can now log in using your new password. You can only edit your own profile. You can track the state of media being processed for your gallery here. You can use
                        <a href="http://daringfireball.net/projects/markdown/basics">
                        Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
                      Markdown</a> for formatting. You can use
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> for formatting. You can use <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> for formatting. You can't delete your only OpenID URL unless you have a password set You can't delete your only Persona email address unless you have a password set. You cannot take action against an administrator You deleted the Blog. You deleted the collection "%s" You deleted the item from the collection. You deleted the media. You have been banned You have to select or add a collection You must be logged in so we know who to send the email to! You must provide a file. You need to confirm the deletion of your account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the You've already verified your email address! Your OAuth clients Your OpenID url was saved successfully. Your Persona email address was saved successfully. Your comment has been posted! Your email address has been verified. Your email address has been verified. You may now login, edit your profile, and submit images! Your last 10 successful uploads Your password was changed successfully an unknown application commented on your post day feature management panel. feed icon hour indefinitely log out minute month newer older unoconv failing to run, check log file until %(until_when)s week year {files_uploaded} out of {files_attempted} files successfully submitted ← Newer ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Browsing media by <a href="%(user_url)s">%(username)s</a> Project-Id-Version: GNU MediaGoblin
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-07-10 12:32-0500
PO-Revision-Date: 2014-07-10 17:32+0000
Last-Translator: cwebber <cwebber@dustycloud.org>
Language-Team: Italian (http://www.transifex.com/projects/p/mediagoblin/language/it/)
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 
Segnalazione Commento #%(report_id)s
  
Segnalazione Elem. Multimediale #%(report_id)s
  
Segnalazione Chiusa #%(report_id)s
  
            <a class="button_action" href="http://mediagoblin.readthedocs.org/">Set up MediaGoblin on your own server</a> 
            >Create an account at this site</a>
            or 
❖ Pubblicato da <a href="%(user_url)s"
class="comment_authorlink">%(username)s</a>
  
IL CONTENUTO DI
<a href="%(user_url)s"> %(user_name)s</a>
È STATO ELIMINATO
  
        Yes. If you would prefer, you may go to the media homepage of the piece
        of media you would like to feature or unfeature and look at the bar to
        the side of the media entry. If the piece of media has not been featured
        yet you should see a button that says 'Feature'. Press that button and
        the media will be featured as a Primary Feature at the top of the page.
        All other featured media entries will remain as features, but will be
        pushed further down the page.<br /><br />

        If you go to the media homepage of a piece of media that is currently
        featured, you will see the options "Unfeature", "Promote" and "Demote"
        where previously there was the button which said "Feature". Click
        Unfeature and that media entry will no longer be displayed on the
        front page, although you can feature it again at any point. Promote
        moves the featured media higher up on the page and makes it more
        prominent and Demote moves the featured media lower down and makes it
        less prominent.
     
        ❖ Reported media by <a href="%(user_url)s">%(user_name)s</a>
     
      Go to the page of the media entry you want to feature. Copy it's URL and
      then paste it into a new line in the text box above. There should be only
      one url per line. The url that you paste into the text box should be under
      the header describing how prominent a feature it will be (whether Primary,
      Secondary, or Tertiary). Once all of the media that you want to feature are
      inside the text box, click the Submit Query button, and your media should be
      displayed on the front page.
     
      These categories just describe how prominent a feature will be on your
      front page. Primary Features are placed at the top of the front page and are
      much larger. Next are Secondary Features, which are slightly smaller.
      Tertiary Features make up a grid at the bottom of the page.<br /><br />

      Primary Features also can display longer descriptions than Secondary
      Features, and Secondary Features can display longer descriptions than
      Tertiary Features. 
      Unfeature a media by removing its line from the above textarea and then
      pressing the Submit Query button.
     
      When a media entry is featured, the entry's title, it's thumbnail and a
      portion of its description will be displayed on your website's front page.
      The number of characters displayed varies on the prominence of the feature.
      Primary Features display the first 512 characters of their description,
      Secondary Features display the first 256 characters of their description,
      and Tertiary Features display the first 128 characters of their description.
     
      When copying and pasting urls into the above text box, be aware that if
      you make a typo, once you press Submit Query, your media entry will NOT be
      featured. Make sure that all your intended Media Entries are featured.
     
    Here you can look up open reports that have been filed by users.
   
Qui puoi cercare gli utenti per intraprendere azioni punitive nei loro confronti.
  
Demote Feature  
Feature Media  
Promote Feature  
Unfeature Media   Blog Dashboard   No blog post yet.  ad accedere al tuo account? "%s" aggiunto alla raccolta "%s" "%s" è già nella raccolta "%s" # di Commenti Pubblicati %(blog_owner_name)s's Blog %(collection_title)s (raccolta di %(username)s) %(collection_title)s di <a href="%(user_url)s">%(username)s</a> %(formatted_time)s fa %(username)s's Privileges raccolte di %(username)s File multimediali di %(username)s Profilo di %(username)s (elimina) + -- Seleziona -- -----------{display_type}-Features---------------------------
 Account di <a href="%(user_url)s">%(user_name)s</a> raccolte di <a href="%(user_url)s">%(username)s</a> File multimediali di <a href="%(user_url)s">%(username)s</a> File multimediali di <a href="%(user_url)s">%(username)s</a> con tag <a href="%(tag_url)s">%(tag)s</a> <em> Go to list view </em> <h2>Invia una Segnalazione</h2> <strong>Confidential</strong> - The client can
                make requests to the GNU MediaGoblin instance that can not be
                intercepted by the user agent (e.g. server-side client).<br />
                <strong>Public</strong> - The client can't make confidential
                requests to the GNU MediaGoblin instance (e.g. client-side
                JavaScript client). Questo utente ha già una raccolta con quel titolo. Impostazioni del profilo salvate Azione Intrapresa Active Reports Filed Active Reports on %(username)s Utenti Attivi Aggiungi Add Blog Post Aggiungi un indirizzo email Persona Aggiungi una raccolta Aggiungi un commento Aggiungi una nuova raccolta Aggiungi un OpenID Aggiungi allegato Aggiungi file multimediali Add new Row Aggiungi questo commento Aggiungi a una raccolta Aggiungi il tuo file multimediale Aggiungi “%(media_title)s” a una raccolta Aggiunto All reports on %(username)s All reports that %(username)s has filed Tutti i diritti riservati Consenti Quasi finito! Il tuo account deve ancora essere attivato. An acceptable processing file was not found Ti è stata inviata un'email con le istruzioni per cambiare la tua password. In breve dovresti ricevere un email contenente istruzioni su come fare. Questo utente ha già un elemento con quel titolo. Si è verificato un errore Le applicazioni che accedono al tuo account possono: Atom feed Allegati Autorizzazione Autorizzazione Completata Autorizzazione Finita Autorizza BANNED until %(expiration_date)s Richiesta Non Valida Ban User Banna l'utente Banned Indefinitely Biografia Sfoglia le raccolte CAUTION: Cookie CSRF non presente. Questo è dovuto a un plugin che blocca i cookie o a qualcosa del genere.<br/>Assicurati di permettere le impostazioni dei cookie per questo dominio. Annulla Non riesco a collegarmi al tema... nessun tema impostato
 Cambia le impostazioni dell'account Cambiare le tue informazioni Cambia la tua password. Stai cambiando le impostazioni dell'account di %(username)s Stai cambiando l'email di %(username)s Stai cambiando la password di %(username)s Clear empty Rows Segnalazioni Chiuse Inserito in Raccolta Raccolta "%s" aggiunta! Comment Anteprima Commento Copia e incolla questo nel tuo client: Copying to public storage failed. Could not link "%s": %s exists and is not a symlink
 Impossibile leggere il file immagine. Impossibile inviare l'email di recupero password perchè il tuo nome utente è inattivo o il tuo indirizzo email non è stato verificato. Non ho trovato nessuno con quel nome utente. Crea Create a Blog Crea un account! Crea una nuova raccolta Creane uno qui! Create/Edit a Blog Create/Edit a Blog Post. Create/Edit a blog post. Creato Elimina Delete Blog Elimina un indirizzo email Persona Elimina un OpenID Delete collection %(collection_title)s Elimina il mio account Elimina definitivamente Elimina il contenuto Demote Nega Descrizione Description of Report Descrizione di questa raccolta Descrizione di questo lavoro Vuoi autorizzare Non hai ancora un account? Non ne hai già uno? E' semplice! Don't process eagerly, pass off to celery Scarica Scarica il modello Modifica Edit Blog Edit Metadata Modifica profilo Stai modificando %(collection_title)s Stai modificando %(media_title)s Stai modificando il profilo di %(username)s Stai modificando gli allegati di %(media_title)s Email Indirizzo email Inviami messaggi email quando altre persone commentano i miei file multimediali E' necessario verificare l'indirizzo email Abilita le notifiche degli eventi nel sito. Enter the URL for the media to be featured Inserisci la vecchia password per dimostrare di essere il proprietario dell'account. Inserisci la tua password per dimostrare di essere il proprietario di questo account. Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded. Esplora FAIL: Local file {filename} could not be accessed.
{filename} will not be uploaded. FAIL: This file is larger than the upload limits for this site. FAIL: This file will put this user past their upload limits. FAIL: This user is already past their upload limits. Feature Feature management panel File Formato del File File Report  File at {path} not found, use -h flag for help For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html> Hai dimenticato la tua password? Prospetto Vai alla pagina: Granted Qui puoi seguire lo stato dei file multimediali in fase di elaborazione in questa istanza. Ecco un posto dove raccontare agli altri di te. Ciao %(username)s,

per attivare il tuo account GNU MediaGoblin, apri il seguente URL nel 
tuo navigatore web.

%(verification_url)s Ciao %(username)s,
%(comment_author)s ha commentato il tuo post (%(comment_url)s) su %(instance_name)s
 Ciao, benvenuto in questo sito MediaGoblin! Salve,

Vogliamo verificare che tu sia %(username)s. Se è così, allora 
per favore segui il link sottostante per verificare il tuo nuovo indirizzo email.

%(verification_url)s

Se tu non sei %(username)s oppure non hai richiesto un cambio dell'email, puoi ignorare questo messaggio. How does this work? How to decide what information is displayed when a media entry is
    featured? How to feature media? How to unfeature a piece of media? However, old link directory symlink found; removed.
 Sono sicuro di volerlo eliminare Sono sicuro di voler eliminare questo elemento dalla raccolta ID Identifier If that email address (case sensitive!) is registered an email has been sent with instructions on how to change your password. Se sei quella persona ma hai perso l'email di verifica, puoi <a href="%(login_url)s">accedere</a> e rispedirla. If you would like to feature a
        piece of media, go to that media entry's homepage and click the button
        that says Image for %(media_title)s Image of goblin stressing out Nel caso non fosse: Includi una nota Nome utente o indirizzo email non valido. File non valido per il tipo di file multimediale indicato. Is there another way to manage featured media? Ultimi 10 caricamenti riusciti Licenza Licenza preferita Posizione Accedi Accedi per creare un account! Esci Accesso fallito! Segna tutti come letti Dimensione massima del file: {0} mb File multimediali in elaborazione Pannello di elaborazione file multimediali File taggati con: %(tag_name)s Simbolo di MediaGoblin MetaData Metadata Metadata for "%(media_name)s" File multimediali più recenti Devi specificare un oauth_token. Nome Name of user these media entries belong to Nuovi commenti Nuovo indirizzo email Nuova password No Nessun servizio OpenID è stato trovato per %s No active reports filed on %(username)s No asset directory for this theme
 No closed reports found. Nessuna elaborazione fallita! Nessun file multimediale in elaborazione No open reports found. Finora nessun elemento elaborato! No request token found. Nessun utente trovato. Nothing is currently featured. Connessioni client OAuth Altezza Oggetto Colpevole Old link found for "%s"; removing.
 Password vecchia Più vecchio → Oops! Oops, il tuo commento era vuoto. OpenID OpenID è stato rimosso con successo. Gli OpenID Operazione non consentita O accedi con OpenID! O accedi con Persona! O accedi con una password! O registrati con OpenID! O registrati con Persona! Originale File originario File PDF Password Path to the csv file containing metadata information. Persona's Prospettiva Per favore controlla i tuoi elementi e riprova. Pubblica nuovi file multimediali a nome mio Realizzato con <a href="http://mediagoblin.org/" title='Version %(version)s'>MediaGoblin</a>, un progetto <a href="http://gnu.org/">GNU</a>. Primary Privilege Cambiamenti del profilo salvati Promote RISOLTO Vuoi davvero eliminare %(title)s? Really delete collection: %(title)s? Vuoi eliminare definitivamente l'utente '%(user_name)s' e tutti i file multimediali/commenti correlati? Eliminare definitivamente %(media_title)s da %(collection_title)s? Motivazione Motivazione della Segnalazione Recupera Password URI di Reindirizzamento Rilasciato con licenza <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPL</a>. <a href="%(source_link)s">Codice sorgente</a> disponibile. Elimina Remove %(media_title)s from %(collection_title)s Segnala Report #%(report_number)s Report ID Pannello di gestione delle segnalazioni Report media Report panel Segnalato Da Reported Comment Reported Content Reported Media Entry Commento segnalato Segnalare questo Commento Segnalare questo Elemento Multimediale Rispedisci email di verifica Email di verifica rispedita. Risolvi Risolvi Questo Problema Resolved Ritorna al Pannello Segnalazioni Return to Users Panel Salva Salva le modifiche Secondary Controlla le tue informazioni (ad es. il profilo, i file multimediali, ecc...) Invia istruzioni Invia un messaggio all'utente Separa i tag con la virgola. Imposta password Imposta la nuova password Lato Iscriviti per creare un account! Skipping "%s"; already set up.
 Titolo Someone has registered an account with this username, but it still has
        to be activated. Qualcuno ha registrato un account con questo nome utente, ma deve ancora essere attivato. Scusa Dave, non posso lasciartelo fare!</p><p>Hai cercato di eseguire una funzione non autorizzata. Stavi provando a eliminare di nuovo tutti gli account utente? Mi dispiace, non supporto questo tipo di file :( Siamo spiacenti, un utente con quell'indirizzo email esiste già. Spiacente, esiste già un utente con quel nome. Mi dispiace, esiste già un account registrato con quell'OpenID. Mi dispiace, esiste già un account registrato con quell'indirizzo email Persona. Mi dispiace, esiste già un account registrato con quell'indirizzo email Persona. Mi dispiace, l'autenticazione è disabilitata in questa istanza. Mi dispiace, i commenti sono disabilitati. Spiacente, segnalazione non trovata. Sorry, no such user found. Sorry, no user by username '{username}' exists Spiacente, la registrazione è disabilitata su questa istanza. Spiacente, le segnalazioni sono disabilitate in questa istanza. Mi dispiace, il server OpenID non è stato trovato Spiacente, il file è troppo grande. Sorry, this audio will not work because
	your web browser does not support HTML5
	audio. Spiacente ma è impossibile leggere questo file audio perché
	il tuo browser web non supporta l'HTML5 
	audio. Sorry, this video will not work because
          your web browser does not support HTML5 
          video. Mi dispiace, questo video non funzionerà perché
      il tuo browser web non supporta i video in
      HTML5. Spiacente, caricando questo file supereresti il tuo limite di memoria. Spiacente, hai raggiunto il limite di memoria disponibile. Stato Rimani connesso Subscribed to comments on %s! Successfully submitted {filename}!
Be sure to look at the Media Processing Panel on your website to be sure it
uploaded successfully. Taggato con Tags Togli il privilegio Termini di Servizio Tertiary Quell'OpenID non è registrato a questo account. Quell'indirizzo email Persona non è registrato in questo account. L'indirizzo email Persona è stato rimosso con successo. The blog was not deleted because you have no rights. Il client {0} è stato registrato! La raccolta non è stata eliminata perchè non hai confermato di essere sicuro. L'elemento non è stato eliminato perchè non hai confermato di essere sicuro. Il file non è stato eliminato perchè non hai confermato di essere sicuro. Nome del client OAuth L'URI di reindirizzamento per le applicazioni, questo campo
è <strong>richiesto</strong> per i client pubblici. La richiesta inviata al server non è valida, per favore ricontrolla Il titolo non può essere vuoto Il titolo non può essere vuoto Il titolo fa parte dell'indirizzo di questa raccolta. Nella maggior parte dei casi non c'è bisogno di cambiarlo. Il titolo fa parte dell'indirizzo del file. Nella maggior parte dei casi non c'è bisogno di cambiarlo. L'id utente è sbagliato. La chiave di verifica o l'id utente è sbagliato La chiave di verifica o l'id utente è sbagliato. Mi dispiace! Sembra che non esista una pagina web con questo indirizzo.</p><p>Se ritieni che l'indirizzo sia corretto, forse la pagina che stai cercando è stata spostata o eliminata. Sembra che non ci sia ancora nessun file multimediale qui... L'elaborazione di questi file caricati è fallita: Questo indirizzo contiene errori Questo campo non accetta indirizzi email. Questo campo è richiesto per i client pubblici Questo campo richiede un indirizzo email. I tuoi file multimediali appariranno qui, ma sembra che tu non abbia ancora aggiunto niente. Questo sito sta utilizzando <a href="http://mediagoblin.org">Mediagoblin</a>, un ottimo programma per caricare e condividere file multimediali. Questo utente non ha (ancora) compilato il proprio profilo. This will be visible to users allowing your
                application to authenticate as them. Questa sarà la tua licenza predefinita nei moduli di caricamento dei file. Titolo Per aggiungere i tuoi file multimediali, scrivere commenti e altro puoi accedere con il tuo account MediaGoblin. Pianta Tipo UnBan User Unfeature Update Metadata Pannello di gestione degli utenti Pannello utente L'utente verrà bannato fino al: User: %(username)s Nome utente Nome utente o indirizzo Email Nome utente o indirizzo email Value Verifica annullata Verifica di %s fallita: %s Verifica la tua email! Transcodifica video fallita View Visualizza tutti i file multimediali di %(username)s View most recent media Visualizza su <a href="%(osm_url)s">OpenStreetMap</a> Avvertimento da parte di WebGL File WebM (VP8/Vorbis) File WebM (codec Vorbis) Sito web Quali azioni intraprenderai per risolvere il problema? What is a Primary Feature? What is a Secondary Feature? Quali privilegi toglierai? When Joined When Reported Perché stai bannando questo Utente? Evviva! Caricato! Woohoo! edited blogpost is submitted Password errata Yes Sì, elimina definitivamente il mio account Hai aggiunto l'allegato %s! Hai già una raccolta che si chiama "%s"! Sei Bannato. Stai eliminando un elemento dalla raccolta di un altro utente. Procedi con cautela. You are about to delete another user's Blog. Proceed with caution. Stai eliminando la raccolta di un altro utente. Procedi con cautela. Stai eliminando un file multimediale di un altro utente. Procedi con attenzione. Stai modificando il profilo di un utente. Procedi con attenzione. Stai modificando la raccolta di un altro utente. Procedi con cautela. Stai modificando file multimediali di un altro utente. Procedi con attenzione. Sei connesso come Non sei più un utente attivo. Per favore contatta l'amministratore di sistema per riattivare il tuo account. You can get a modern web browser that
	can play the audio at <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! Puoi scaricare un browser web moderno,
	  in grado di leggere questo file audio, qui <a href="http://getfirefox.com">
	  http://getfirefox.com</a>! You can get a modern web browser that 
          can play this video at <a href="http://getfirefox.com">
          http://getfirefox.com</a>! Qui <a href="http://getfirefox.com">http://getfirefox.com</a>
puoi scaricare un browser web moderno, in grado di visualizzare
questo video! Ora puoi effettuare l'accesso con la nuova password. Puoi modificare solo il tuo profilo. Qui controlli lo stato dei file multimediali in elaborazione per la tua galleria. Puoi utilizzare il
<a href="http://daringfireball.net/projects/markdown/basics">
Markdown</a> per la formattazione. Puoi usare il
<a href="http://daringfireball.net/projects/markdown/basics" target="_blank">
Markdown</a> per la formattazione. Puoi usare il
                      <a href="http://daringfireball.net/projects/markdown/basics">
                      Markdown</a> per la formattazione. Puoi usare il <a href="http://daringfireball.net/projects/markdown/basics" target="_blank">Markdown</a> per la formattazione. Non puoi eliminare il tuo unico URL OpenID se prima non hai impostato una password Non puoi eliminare il tuo unico indirizzo email Persona se prima non hai impostato una password. Non puoi agire contro un amministratore You deleted the Blog. Hai eliminato la raccolta "%s" Hai eliminato l'elemento dalla raccolta. Hai eliminato il file. Sei stato bannato Devi selezionare o aggiungere una raccolta Devi effettuare l'accesso così possiamo sapere a chi inviare l'email! Devi specificare un file. Devi confermare l'eliminazione del tuo account. You will not receive notifications for comments on %s. You're seeing this page because you are a user capable of
        featuring media, a regular user would see a blank page, so be sure to
        have media featured as long as your instance has the 'archivalook'
        plugin enabled. A more advanced tool to manage features can be found
        in the Hai già verificato il tuo indirizzo email! I tuoi client OAuth Il tuo url OpenID è stato salvato con successo. Il tuo indirizzo email Persona è stato salvato con successo. Il tuo commento è stato aggiunto! Il tuo indirizzo email è stato verificato. Il tuo indirizzo email è stato verificato. Ora puoi accedere, modificare il tuo profilo e caricare immagini! I tuoi ultimi 10 caricamenti riusciti La tua password è stata cambiata con successo un'applicazione sconosciuta ha commentato il tuo post giorno feature management panel. feed icon ora indefinitamente esci minuto mese più recente più vecchio unoconv failing to run, check log file fino al %(until_when)s settimana anno {files_uploaded} out of {files_attempted} files successfully submitted ← Più recente ❖ Blog post by <a href="%(user_url)s">%(username)s</a> ❖ Stai guardando i file multimediali di <a href="%(user_url)s">%(username)s</a> 